﻿using Microsoft.AspNetCore.Components;
using SharedData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Json;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading.Tasks;

namespace UniversalGameServerManager
{
    public class InternalManager
    {

        public static async void PollAllServers(object sender, System.Timers.ElapsedEventArgs e)
        {

            if (!GlobalVariables.gIsIntialized && !isUserOnline)
                return;

            foreach (var item in GlobalVariables.gDedicatedServers)
            {
                if (item.isOnline)
                {
                    foreach (var ipAddress in item.IPAddress)
                    {

                        IPAddress ip;
                        bool isParsed = IPAddress.TryParse(ipAddress, out ip);

                        if (isParsed)
                        {
                            if (ip.AddressFamily == AddressFamily.InterNetwork)
                            {
                                try
                                {
                                    var stat = await GlobalVariables.gHttpClient.GetFromJsonAsync<Stastistic>("http://" + ipAddress + ":5000/api/v1/DedicatedServer/GetLatestDedicatedStat");


                                    item.DedicatedServerStats.Add(stat);
                                    break;
                                }
                                catch (Exception exception)
                                {
                                    Console.WriteLine(exception.Message);
                                }
                            }
                        }
                    }
                }
                else
                {
                    continue;
                }
            }
        }

        //foreach (var item in GlobalVariables.gTerrariaServers)
        //{
        //    TcpClient tcpClient = new TcpClient();
        //    try
        //    {
        //        tcpClient.Connect(item.IPAddress, (int)item.PortNumber);
        //        tcpClient.Close();
        //        Console.WriteLine("Port open");
        //    }
        //    catch (Exception)
        //    {
        //        Console.WriteLine("Port closed");
        //    }
        //}
        //Console.WriteLine("b4 POLL");

        //if (Pages.GameServerPages.Console.isPollEnabled)
        //{

        //    var httpResult = await GlobalVariables.gHttpClient.PostAsJsonAsync<ulong>(Pages.GameServerPages.Console.IPAddress + "api/v1/Console/GetConsoleInfo", Pages.GameServerPages.Console.m_GameModel.UniqueGameServerID);

        //    var newText = await httpResult.Content.ReadFromJsonAsync<List<string>>();

        //    foreach (var item in newText)
        //    {
        //        Console.WriteLine(item);
        //        Pages.GameServerPages.Console.m_ConsoleText.Add(item);
        //    }


        //}

        //Console.WriteLine("after POLL");
        public static bool isUserOnline
        {
            get
            {
                try
                {
                    Ping myPing = new Ping();
                    String host = "google.com";
                    byte[] buffer = new byte[32];
                    int timeout = 1000;
                    PingOptions pingOptions = new PingOptions();
                    PingReply reply = myPing.Send(host, timeout, buffer, pingOptions);
                    return (reply.Status == IPStatus.Success);
                }
                catch (Exception)
                {
                    return false;
                }
            }
        }
    }


}

