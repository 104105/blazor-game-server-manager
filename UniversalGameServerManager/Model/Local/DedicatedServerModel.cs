﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace UniversalGameServerManager.Model.Local
{
    public class DedicatedServerModel
    {
        public string Name { get; set; }
        public string CPU { get; set; }
        public float CPUFrequency { get; set; }
        public int CPUCores { get; set; }
        public ulong MemoryMB { get; set; }
        public float NetworkSpeed { get; set; }
        [IgnoreDataMember]
        public bool isOnline
        {
            get
            {
                if (DedicatedServerStats != null && DedicatedServerStats.Count > 0)
                {
                    return ((DateTime.UtcNow - DedicatedServerStats.Last().CaptureTime).TotalSeconds < 20);
                }else
                {
                    return false;
                }
            }
        }
        // public List<HardDrive> HardDrives { get; set; }
        /*  public ulong TotalHardDriveSpace
          {
              get
              {
                  ulong val = 0;
                  foreach (var item in HardDrives)
                  {
                      val += item.TotalSizeMB;
                  }
                  return val;
              }
          }*/
        public List<string> IPAddress { get; set; } 
        [IgnoreDataMember]
        public List<Stastistic> DedicatedServerStats { get; set; }
    }
}
