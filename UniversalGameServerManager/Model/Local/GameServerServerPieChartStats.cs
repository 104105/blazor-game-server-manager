﻿namespace UniversalGameServerManager
{
    public class GameServerServerPieChartStats
    {
        public GameServerServerPieChartStats()
        {
        }

        public string xName { get; set; }
        public int yNumber { get; set; }
        public string Fill { get; set; }
    }
}