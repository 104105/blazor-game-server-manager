﻿namespace UniversalGameServerManager
{
    public class DedicatedServerPieChartStats
    {
        public DedicatedServerPieChartStats()
        {
        }

        public string xName { get; set; }
        public int yNumber { get; set; }
        public string Fill { get; set; }

    }
}