﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;
using Console = System.Console;

namespace UniversalGameServerManager.Model.Local
{
   public class GameServerModel
    {
        public ulong UniqueGameServerID { get; set; }
        public string ServerName { get; set; }
        public string GameName { get; set; }
        public int CurrentPlayers { get; set; }
        public int MaxPlayers { get; set; }
        public int GameID { get; set; }
        public string NetworkAddress { get; set; }
        public bool isRunning
        {
            get
            {
                var item = GlobalVariables.FindDedicatedServer(NetworkAddress);


                if (item != null)
                {
                    if (item.DedicatedServerStats != null &&item.DedicatedServerStats.Count > 0) { var stats = item.DedicatedServerStats[item.DedicatedServerStats.Count - 1];
                        if (stats.isGameServerOnline.ContainsKey(UniqueGameServerID.ToString()))
                        {
                            return bool.Parse(stats.isGameServerOnline[UniqueGameServerID.ToString()]);
                        }
                        else
                        {
                            Console.WriteLine("ERROR, dedicated server does not list gameserver with ID " + UniqueGameServerID);
                            return false;
                        } }
                    else
                    {
                        Console.WriteLine("Stats for Dedicated Server Not Collected " + UniqueGameServerID);
                        return false;
                    }
                }else
                {
                    Console.WriteLine("Can't find dedicated server associated with this game server " + UniqueGameServerID);
                    return false;
                }
            }
            set
            {

            }
        }
        

        public GameServerModel() { }

        public GameServerModel(CounterStrikeGOGameServer csgo)
        {
            ServerName = csgo.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.CounterStrikeGO];
            CurrentPlayers = 0;
            MaxPlayers = csgo.MaxPlayers ?? 0;
            GameID = (int)SharedData.GameID.CounterStrikeGO;
            UniqueGameServerID = csgo.UniqueGameServerID;
            NetworkAddress = csgo.IPAddress;
        }

        public GameServerModel(TerrariaGameServer terraria)
        {
            ServerName = terraria.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.Terraria];
            CurrentPlayers = 0;
            MaxPlayers = (int)terraria.MaxPlayers;
            GameID = (int)SharedData.GameID.Terraria;
            UniqueGameServerID = terraria.UniqueGameServerID;
            NetworkAddress = terraria.IPAddress;
        }

        public GameServerModel(_7Days2DieGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.SevenDays2Die];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.SevenDays2Die;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(ArkGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.ArkSurvivalEvolved];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.ArkSurvivalEvolved;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(AstroneerGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.Astroneer];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.Astroneer;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(ConanExilesGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.ConanExiles];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.ConanExiles;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(CounterStrikeGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.CounterStrike];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.CounterStrike;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(CounterStrikeConditionZeroGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.CounterStrikeConditionZero];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.CounterStrikeConditionZero;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(CounterStrikeSourceGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.CounterStrikeSource];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.CounterStrikeSource;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(EmpyrionGalacticSurvivalGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.EmpyrionGalacticSurvival];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.EmpyrionGalacticSurvival;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(GarrysModGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.GarrysMod];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.GarrysMod;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(HellLetLooseGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.HellLetLoose];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.HellLetLoose;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(KillingFloor2GameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.KillingFloor2];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.KillingFloor2;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(Left4Dead2GameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.Left4Dead2];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.Left4Dead2;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(MordhauGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.Mordhau];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.Mordhau;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(ProjectZomboidGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.ProjectZomboid];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.ProjectZomboid;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(RustGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.Rust];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.Rust;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(SCPSecretLaboratoryGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.SCPSecretLaboratory];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.SCPSecretLaboratory;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(SpaceEngineersGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.SpaceEngineers];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.SpaceEngineers;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(SquadGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.Squad];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.Squad;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(TeamFortress2GameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.TeamFortress2];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.TeamFortress2;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(TheForestGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.TheForest];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.TheForest;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }

        public GameServerModel(TheIsleGameServer data)
        {
            ServerName = data.ServerName;
            GameName = GameIDHelpers.GameIDToGameNames[SharedData.GameID.TheIsle];
            CurrentPlayers = 0;
            MaxPlayers = (int)0;
            GameID = (int)SharedData.GameID.TheIsle;
            UniqueGameServerID = data.UniqueGameServerID;
            NetworkAddress = data.IPAddress;
        }



        public GameServerModel(string serverName, string gameName, int currentPlayers, int maxPlayers, int gameID, string networkAddress, ulong uniqueGameServerID)
        {
            ServerName = serverName;
            GameName = gameName;
            CurrentPlayers = currentPlayers;
            MaxPlayers = maxPlayers;
            GameID = gameID;
            NetworkAddress = networkAddress;
            UniqueGameServerID = uniqueGameServerID;
        }
    }

}
