﻿using System.Collections.Generic;

namespace UniversalGameServerManager.Models
{
    public class NavigationMenuData
        {
            public string Text { get; set; }
            public string Value { get; set; } = "index";
            public string Icon { get; set; }

        public static List<NavigationMenuData> data = new List<NavigationMenuData>() { new NavigationMenuData("Dashboard", "index", "icon-dashboard"), new NavigationMenuData("Dedicated Servers", "DedicatedServers", "oi oi-browser"),new NavigationMenuData("GameServers", "Game Servers", "oi oi-eye"),new NavigationMenuData("Events", "Events", "oi oi-envelope-closed")
        };

        public NavigationMenuData()
            {
            data = GetNavigationItems();
            }
            public NavigationMenuData(string Text, string Value, string Icon)
            {
                this.Text = Text;
                this.Value = Value;
                this.Icon = Icon;
            }
            public List<NavigationMenuData> GetNavigationItems()
            {
                data.Add(new NavigationMenuData("Dashboard", "index", "icon-dashboard"));
                data.Add(new NavigationMenuData("Dedicated Servers", "DedicatedServers", "oi oi-browser"));
                data.Add(new NavigationMenuData("GameServers", "Game Servers", "oi oi-eye"));
                data.Add(new NavigationMenuData("Events", "Events", "oi oi-envelope-closed"));
           
                return data;
            }
    }

}
