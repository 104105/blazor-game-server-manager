﻿using SharedData;
using System.Collections.Generic;
using System.Net.Http;
using UniversalGameServerManager.Model.Local;
using UniversalGameServerManager;
using Blazored.LocalStorage;
using System.Net.Http.Json;
using UniversalGameServerManager.Model;
using System;
using System.Collections.ObjectModel;
using Syncfusion.Blazor.Navigations;
using System.Linq;
using UniversalGameServerManager.Pages;

namespace UniversalGameServerManager
{
    public class GlobalVariables
    {

        public static List<DedicatedServerModel> gDedicatedServers  { get; set; }
        public static DedicatedServerModel gSelectedDedicatedServer { get; set; }
        public static string gCurrentSelectedIPAddressForGameServer { get; set; }
        public static ISyncLocalStorageService m_LocalStorage { get; set; }
        public static HttpClient gHttpClient { get; set; }
        public static SettingsModel gSettings { get; set; }
        public static bool gIsIntialized { get; set; }
        public static DedicatedServerPieChartStats gOfflineDedicatedServerStats
        {
            get;set;
        }

        public static DedicatedServerPieChartStats gOnlineDedicatedServerStats
        {

            get; set;
        }

        public static GameServerServerPieChartStats gOfflineGameServerStats
        {

            get; set;
        }

        public static GameServerServerPieChartStats gOnlineGameServerStats
        {

            get; set;
        }

        public static bool Initialize()
        {
            GlobalVariables.gDedicatedServers = LoadDedicatedServerData();
            InitializeDedicatedServers();
            LoadSettingsData();
             BuildUIModels();
            return true;
        }

        static void LoadSettingsData()
        {
            if (m_LocalStorage.ContainKey("Settings"))
            {
                gSettings = m_LocalStorage.GetItem<SettingsModel>("Settings");
            }
            else
            {
                gSettings = new SettingsModel(true);// List<CounterStrikeGOGameServer>();
            }
        }
        static void BuildUIModels()
        {
           GameServers.gGameServerModels = new List<GameServerModel>();


            foreach (var item in gSettings.g7Days2DieGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }
            foreach (var item in gSettings.gArkGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gAstroneerGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gConanExilesGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gCounterStrikeGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gCounterStrikeConditionZeroGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }


            foreach (var item in gSettings.gCounterStrikeGoGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }
         
            foreach (var item in gSettings.gCounterStrikeSourceGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gEmpyrionGalacticSurvivalGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gGarrysModGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gHellLetLooseGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gKillingFloor2GameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gLeft4Dead2GameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gMordhauGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gProjectZomboidGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gRustGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gSCPSecretLaboratoryGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gSpaceEngineersGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gSquadGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gTerrariaGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }


            foreach (var item in gSettings.gTerrariaGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }
          
            foreach (var item in gSettings.gTF2GameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gTheForestGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }

            foreach (var item in gSettings.gTheIsleGameServers)
            {
                GameServers.gGameServerModels.Add(new GameServerModel(item));
            }



        }

        public static bool OnExit()
        {
            SaveDedicatedServersData();
            SaveGameServersData();
            return true;
        }


        public static void SaveDedicatedServersData()
        {    
           //  File.WriteAllText("DedicatedServers.json", JsonConvert.SerializeObject(GlobalVariables.gDedicatedServers));
            m_LocalStorage.SetItem("DedicatedServers", GlobalVariables.gDedicatedServers);
        }

        public static void SaveGameServersData()
        {
            //if (GlobalVariables.gCounterStrikeGlobaloffensiveServers.Count > 0)
            //{
            //    //  File.WriteAllText("CounterStrikeGlobalOffensiveServers.json", JsonConvert.SerializeObject(GlobalVariables.gCounterStrikeGlobaloffensiveServers));

            //    m_LocalStorage.SetItem("CounterStrikeGlobalOffensiveServers", GlobalVariables.gCounterStrikeGlobaloffensiveServers);
            //}

            //if (GlobalVariables.gTerrariaServers.Count > 0)
            //{
            //    //  File.WriteAllText("CounterStrikeGlobalOffensiveServers.json", JsonConvert.SerializeObject(GlobalVariables.gCounterStrikeGlobaloffensiveServers));

            //    m_LocalStorage.SetItem("TerrariaServers", GlobalVariables.gTerrariaServers);
            //}

            m_LocalStorage.SetItem("Settings", gSettings);
        }

        public static List<DedicatedServerModel> LoadDedicatedServerData()
        {

            // return JsonConvert.DeserializeObject<List<DedicatedServerModel>>(
            //     File.ReadAllText("DedicatedServers.json"));
            if (m_LocalStorage.ContainKey("DedicatedServers"))
            {

                return m_LocalStorage.GetItem<List<DedicatedServerModel>>("DedicatedServers");
            }else
            {
                return new List<DedicatedServerModel>();
            }
          
        }

        public static async void InitializeDedicatedServers() {
            foreach (var item in gDedicatedServers)
            {
                try
                {
                    var syncInfo = await gHttpClient.GetFromJsonAsync<SyncInfo>("http://" + item.IPAddress[1] + ":5000/api/v1/DedicatedServer/GetSyncInfo");

                    item.DedicatedServerStats = syncInfo.DediStats;

                }
                catch (Exception exception)
                {

                    Console.WriteLine("Exception: " + exception.Message + "    " + exception.TargetSite.ToString());
                    return;
                }
            }

            int numberOnline = 0;
            int numberOffline = 0;
            int numberGameServersOnline = 0;
            int numberGameServerOffline = 0;

            foreach (var item in GlobalVariables.gDedicatedServers)
            {


                if (item.isOnline)
                {
                    numberOnline++;
                    foreach (var gameServer in item.DedicatedServerStats[item.DedicatedServerStats.Count - 1].isGameServerOnline)
                    {
                        if (gameServer.Value.Equals(bool.TrueString))
                        {
                            numberGameServersOnline++;
                        }
                        else
                        {
                            numberGameServerOffline++;
                        }
                    }
                }
                else
                {
                    numberOffline++;
                }
            }

            gOfflineDedicatedServerStats = new DedicatedServerPieChartStats()
            {
                xName = numberOnline == 0 && numberOffline == 0 ? "No Servers Setup" : "Offline",
                yNumber = numberOnline == 0 && numberOffline == 0 ? 1: numberOffline,
                Fill = numberOnline == 0 && numberOffline == 0 ? "#808080" : "#ff0000"
            };

            if (!(numberOnline == 0 && numberOffline == 0))
            {
                gOnlineDedicatedServerStats = new DedicatedServerPieChartStats()
                {
                    xName = "Online",
                    yNumber = numberOnline,
                    Fill = "#32CD32"
                };
                Pages.Index.gDedicatedServerStats.Add(gOnlineDedicatedServerStats);
            }

            gOfflineGameServerStats = new GameServerServerPieChartStats()
            {
                xName = numberGameServerOffline == 0 && numberGameServersOnline == 0 ? "No Servers Setup" : "Offline",
                yNumber = numberGameServerOffline == 0 && numberGameServersOnline == 0 ? 1 : numberGameServerOffline,
                Fill = numberGameServerOffline == 0 && numberGameServersOnline == 0 ? "#808080" : "#ff0000"
            };

            if (!(numberGameServerOffline == 0 && numberGameServersOnline == 0))
            {
                gOnlineGameServerStats = new GameServerServerPieChartStats()
                {
                    xName = "Online",
                    yNumber = numberGameServersOnline,
                    Fill = "#32CD32"
                };
                Pages.Index.gGameServerStats.Add(gOnlineGameServerStats);
            }
            try
            {
                Pages.Index.gDedicatedServerStats.Add(gOfflineDedicatedServerStats);
                Pages.Index.gGameServerStats.Add(gOfflineGameServerStats);

                Pages.Index.sfChart.Refresh();
                Pages.Index.gameServersChart.Refresh();
            }
            catch(Exception exception)
            {
                Console.WriteLine("Exception: " + exception.Message + "    " + exception.TargetSite.ToString());
            }
            GlobalVariables.gIsIntialized = true;

        }

        public static void AddOnlineDedicatedServer()
        {
            gOnlineDedicatedServerStats.yNumber++;
        }

        public static void RemoveOnlineDedicatedServer()
        {
            gOnlineDedicatedServerStats.yNumber--;

        }

        public static void AddOfflineDedicatedServer()
        {
            gOfflineDedicatedServerStats.yNumber++;
        }

        public static void RemoveofflineDedicatedServer()
        {
            gOfflineDedicatedServerStats.yNumber--;

        }

        public static void AddOnlineGameServer()
        {
            gOnlineGameServerStats.yNumber++;
        }


        public static void RemoveOnlineGameServer()
        {
            gOnlineGameServerStats.yNumber--;
        }

        public static void AddOfflineGameServer()
        {
            gOfflineDedicatedServerStats.yNumber++;
        }

        public static void RemoveOfflineGameServer()
        {
            gOfflineDedicatedServerStats.yNumber--;
        }

        static List<CounterStrikeGOGameServer> LoadCounterStrikeGlobalOffensiveServerData()
        {
            if (m_LocalStorage.ContainKey("CounterStrikeGlobalOffensiveServers"))
            {
                return m_LocalStorage.GetItem<List<CounterStrikeGOGameServer>>("CounterStrikeGlobalOffensiveServers");
            }
            else
            {
                return new List<CounterStrikeGOGameServer>();
            }
        }

        static List<TerrariaGameServer> LoadTerrariaServerData()
        {
            if (m_LocalStorage.ContainKey("TerrariaServers"))
            {
                return m_LocalStorage.GetItem<List<TerrariaGameServer>>("TerrariaServers");
            }
            else
            {
                return new List<TerrariaGameServer>();
            }
        }

        public static TerrariaGameServer GetTerrariaGameServer(ulong id)
        {
            foreach (var item in gSettings.gTerrariaGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static _7Days2DieGameServer Get7Days2DieGameServer(ulong id)
        {
            foreach (var item in gSettings.g7Days2DieGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }
        public static ArkGameServer GetArkSEGameServer(ulong id)
        {
            foreach (var item in gSettings.gArkGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }
        public static AstroneerGameServer GetAstroneerGameServer(ulong id)
        {
            foreach (var item in gSettings.gAstroneerGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static ConanExilesGameServer GetConanExilesGameServer(ulong id)
        {
            foreach (var item in gSettings.gConanExilesGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }
        public static CounterStrikeGameServer GetCounterStrikeGameServer(ulong id)
        {
            foreach (var item in gSettings.gCounterStrikeGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }
        public static CounterStrikeConditionZeroGameServer GetCounterStrikeConditionZero(ulong id)
        {
            foreach (var item in gSettings.gCounterStrikeConditionZeroGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static CounterStrikeGOGameServer GetCSGoGameServer(ulong id)
        {
            foreach (var item in gSettings.gCounterStrikeGoGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static CounterStrikeSourceGameServer GetCounterStrikeSourceGameServer(ulong id)
        {
            foreach (var item in gSettings.gCounterStrikeSourceGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static EmpyrionGalacticSurvivalGameServer GetEmpyrionGalacticSurvivalGame(ulong id)
        {
            foreach (var item in gSettings.gEmpyrionGalacticSurvivalGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static GarrysModGameServer GetGarrysModGameServer(ulong id)
        {
            foreach (var item in gSettings.gGarrysModGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static HellLetLooseGameServer GetHellLetLooseGameServer(ulong id)
        {
            foreach (var item in gSettings.gHellLetLooseGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static KillingFloor2GameServer GetKillingFloor2GameServer(ulong id)
        {
            foreach (var item in gSettings.gKillingFloor2GameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static Left4Dead2GameServer GetLeft4Dead2GameServer(ulong id)
        {
            foreach (var item in gSettings.gLeft4Dead2GameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static MordhauGameServer GetMordhauGameServer(ulong id)
        {
            foreach (var item in gSettings.gMordhauGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static ProjectZomboidGameServer GetProjectZomboidGameServer(ulong id)
        {
            foreach (var item in gSettings.gProjectZomboidGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static RustGameServer GetRustGameServer(ulong id)
        {
            foreach (var item in gSettings.gRustGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static SCPSecretLaboratoryGameServer GetSecretLaboratoryGameServer(ulong id)
        {
            foreach (var item in gSettings.gSCPSecretLaboratoryGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static SpaceEngineersGameServer GetSpaceEngineersGameServer(ulong id)
        {
            foreach (var item in gSettings.gSpaceEngineersGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static SquadGameServer GetSquadGameServer(ulong id)
        {
            foreach (var item in gSettings.gSquadGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static TeamFortress2GameServer GetTeamFortress2GameServer(ulong id)
        {
            foreach (var item in gSettings.gTF2GameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static TheForestGameServer GetTheForestGameServer(ulong id)
        {
            foreach (var item in gSettings.gTheForestGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }

        public static TheIsleGameServer GetIsleGameServer(ulong id)
        {
            foreach (var item in gSettings.gTheIsleGameServers)
            {
                if (item.UniqueGameServerID == id)
                {
                    return item;
                }
            }
            return null;
        }


        public static DedicatedServerModel FindDedicatedServer(string ip)
        {
            foreach (var item in gDedicatedServers)
            {
                foreach (var ipAddress in item.IPAddress)
                {
                    if (item.IPAddress.Contains(ipAddress))
                    {
                        return item;
                    }
                }
            }

            return null;
        }
    }
}
