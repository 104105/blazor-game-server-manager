using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Syncfusion.Blazor;
using Blazored.LocalStorage;
using Blazored.Toast;
using System.Threading;

namespace UniversalGameServerManager
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
      

                var builder = WebAssemblyHostBuilder.CreateDefault(args);
                builder.RootComponents.Add<App>("app");

                builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
            //     Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("");

                builder.Services.AddSyncfusionBlazor();

                builder.Services.AddBlazoredLocalStorage();
                builder.Services.AddBlazoredToast();
                var builderObj = builder.Build();

                GlobalVariables.m_LocalStorage = (ISyncLocalStorageService)builderObj.Services.GetService(typeof(ISyncLocalStorageService));
                GlobalVariables.gHttpClient = (HttpClient)builderObj.Services.GetService(typeof(HttpClient));
                GlobalVariables.Initialize();

                //Setup timed events
                System.Timers.Timer pollManager = new System.Timers.Timer(5005);

                //Poll server stats
                pollManager.Elapsed += InternalManager.PollAllServers;

                pollManager.AutoReset = true;
                pollManager.Start();



                await builderObj.RunAsync();
           
        }

    
    }
}
