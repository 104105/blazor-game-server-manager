﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedData
{
  public  class BasicInfo
    {
        public string CPU { get; set; }
        public float CPUFrequency { get; set; }
        public int CPUCores { get; set; }
        public ulong MemoryMB { get; set; }
        public float NetworkSpeed { get; set; }
        public List<HardDrive> HardDrives { get; set; }
        public List<string> IPAddress { get; set; }
    }
}
