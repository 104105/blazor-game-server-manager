﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedData
{

    public class GameIDHelpers
    {
        public static Dictionary<GameID, string> GameIDToGameNames = new Dictionary<GameID, string>() {
            { GameID.CounterStrikeGO, "Counter Strike:Global Offensive"},
            { GameID.Terraria,"Terraria" },
            { GameID.ConanExiles,"Conan Exiles" },
            { GameID.SevenDays2Die,"7 Days to Die" },
            { GameID.ArkSurvivalEvolved,"Ark Survival Evolved" },
            { GameID.TeamFortress2,"Team Fortress 2" },
            { GameID.GarrysMod,"Garry's Mod" },
            { GameID.Rust,"Rust" },
            {GameID.KillingFloor2,"Killing Floor 2"  },
            {GameID.TheForest,"The Forest"  },
            {GameID.ProjectZomboid,"Project Zomboid" },
            {GameID.Left4Dead2,"Left 4 Dead 2" },
            {GameID.Squad ,"Squad" },
            {GameID.SpaceEngineers,"Space Engineers" },
            {GameID.HellLetLoose, "Hell Let Loose"},
            {GameID.SCPSecretLaboratory,"SCP Secret Laboratory" },
            {GameID.Mordhau,"Mordhau" },
            {GameID.TheIsle,"The Isle" },
            {GameID.EmpyrionGalacticSurvival,"Empyrion Galactic Survival" },
            {GameID.Astroneer,"Astroneer" },
            {GameID.CounterStrike,"Counter-Strike 1.6" },
            {GameID.CounterStrikeSource,"Counter-Strike Source" },
            {GameID.CounterStrikeConditionZero, "Counter-Strike: Condition Zero" }

        };

        public static Dictionary<string, GameID> GameNamesToGameID = new Dictionary<string, GameID>()
        {
            {"Counter Strike:Global Offensive",GameID.CounterStrikeGO},
            {"Terraria", GameID.Terraria },
            {"Conan Exiles", GameID.ConanExiles },
            {"7 Days to Die", GameID.SevenDays2Die },
            {"Ark Survival Evolved", GameID.ArkSurvivalEvolved },
            {"Team Fortress 2", GameID.TeamFortress2 },
            {"Garry's Mod", GameID.GarrysMod },
            {"Rust", GameID.Rust },
            {"Killing Floor 2" , GameID.KillingFloor2 },
            {"The Forest" , GameID.TheForest },
            {"Project Zomboid" , GameID.ProjectZomboid },
            {"Left 4 Dead 2" , GameID.Left4Dead2 },
            {"Squad" , GameID.Squad },
            {"Space Engineers",GameID.SpaceEngineers },
            {"Hell Let Loose",GameID.HellLetLoose },
            {"SCP Secret Laboratory",GameID.SCPSecretLaboratory },
            {"Mordhau",GameID.Mordhau },
            {"The Isle",GameID.TheIsle },
            {"Empyrion Galactic Survival",GameID.EmpyrionGalacticSurvival },
            {"Astroneer",GameID.Astroneer },
            {"Counter Strike 1.6",GameID.CounterStrike },
            {"Counter-Strike Source",GameID.CounterStrikeSource },
            {"Counter-Strike: Condition Zero",GameID.CounterStrikeConditionZero }
};
    }
    public enum GameID : int
    {
        CounterStrikeGO = 0,
        Terraria = 1,
        ConanExiles = 2,
        SevenDays2Die = 3,
        ArkSurvivalEvolved = 4,
        TeamFortress2= 5,
        GarrysMod = 6,
        Rust =7,
        KillingFloor2 =8,
        TheForest =9,
        ProjectZomboid =10,
        Left4Dead2 =11,
        Squad =12,
        SpaceEngineers =13,
        HellLetLoose =14,
        SCPSecretLaboratory =15,
        Mordhau =16,
        TheIsle=17,
        EmpyrionGalacticSurvival =18,
        Astroneer =19,
        CounterStrike =20,
        CounterStrikeSource =21,
        CounterStrikeConditionZero =22
    }

}
