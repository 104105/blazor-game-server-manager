﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedData
{
    public class ConsoleCommand
    {
        public string Command { get; set; }
        public ulong GameServerID { get; set; }
    }
}
