﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Diagnostics;

namespace SharedData
{
    public static class Helpers
    {

        public static string LinuxTerminalCmd(this string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            Console.WriteLine("Result: " + result);
            return result;
        }
        public static string LinuxTerminalCmd(this string cmd, string cmd1)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");

            var process = new Process()
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    RedirectStandardInput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string result = process.StandardOutput.ReadToEnd();
             process.StandardInput.WriteLine(cmd1);
            string result1 = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
            Console.WriteLine("Result: " + result);
            return result;
        }

        
    }

}
