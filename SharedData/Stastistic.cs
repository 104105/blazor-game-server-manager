﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedData
{
    public class Stastistic
    {
        public float CPUUtilization { get; set; }
        public float MemoryUsage { get; set; }
        public double NetworkDownloadUsage { get; set; }
        public double NetworkUploadUsage { get; set; }
        public DateTime CaptureTime { get; set; }
      //public Dictionary<ulong, bool> isGameServerOnline { get; set; }
        public Dictionary<string, string> isGameServerOnline { get; set; }

    }
}
