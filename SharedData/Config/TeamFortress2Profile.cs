﻿using SharedData.Interfaces;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace SharedData.Config
{
    public class TeamFortress2Profile : IBaseConfig
    {
        public GameID GameID { get; set; }
        public List<ConfigFile> Files { get; set; }
        public string ProfileName { get; set; }

        public TeamFortress2Profile()
        {
            GameID = GameID.TeamFortress2;
        }

        public void GetConfig(string FileName)
        {
            throw new NotImplementedException();
        }

        public void SaveConfig()
        {
            throw new NotImplementedException();
        }
    }
}
