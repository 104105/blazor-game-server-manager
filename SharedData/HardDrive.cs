﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedData
{
    public class HardDrive
    {
       
        public string Label { get; set; }
        public ulong TotalFreeSpaceMB { get; set; }
        public ulong TotalSizeMB { get; set; }
    }
}
