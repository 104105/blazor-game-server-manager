﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Text;

namespace SharedData
{
    public class ConanExilesGameServer
    {
        public int? GameID { get; set; }
        public ulong UniqueGameServerID { get; set; }
        public string ServerName { get; set; }
        public string IPAddress { get; set; }
        public string ServerPassword { get; set; }
        public string AsyncTaskTimeout { get; set; }
        public int? GameServerQueryPort { get; set; }
        public int? Port { get; set; }
        public int? PeerPort { get; set; }
        public int? NetServerMaxTickRate { get; set; }
        public int? MaxPlayers { get; set; }
        public string AdminPassword { get; set; }
        public int? MaxNudity { get; set; }
        public bool PVPBlitzServer { get; set; }
        public bool PVPEnabled { get; set; }
        public int? ServerRegion { get; set; }
        public int? ServerCommunity { get; set; }
        public bool isBattleEyeEnabled { get; set; }

        /*
       AdminPassword	AdminPassword=SecretPassword	Sets the administrative password for the server. This will grant players administrative rights when used from the settings menu in-game.
MaxNudity	MaxNudity=2	Sets the maximum nudity level allowed on the server. (0=None, 1=Partial, 2=Full)
PVPBlitzServer	PVPBlitzServer=False	Enables/disables Blitz mode. (accelerated progression)
PVPEnabled	PVPEnabled=True	Enables/disables PvP on the server.
serverRegion	serverRegion=1	Sets the server's region. (0=EU, 1=NA, 2=Asia)
ServerCommunity	ServerCommunity=3	Sets the server's play style (0=None, 1=Purist, 2=Relaxed, 3=Hard Core, 4=Role Playing, 5=Experimental)
IsBattlEyeEnabled	IsBattlEyeEnabled=False	Enables/disables BattlEye protection for the server.


  ServerName	ServerName=My Server	Sets the name that will be displayed in the server list.
ServerPassword	ServerPassword=Password123	Sets the server password that will need to be entered to join the server. Leave blank for no password.
AsyncTaskTimeout	AsyncTaskTimeout=360	Maximum time in seconds that the server will be allotted to boot. Be sure to increase this value on servers with weak hardware or large databases.
GameServerQueryPort	GameServerQueryPort=27015	Sets the query port for Steam matchmaking. Same as setting -QueryPort in the command line.

        Port	Port=7777	Sets the game server port.
PeerPort	PeerPort=7778	Sets the peer port for the game server.

        NetServerMaxTickRate	NetServerMaxTickRate=30	Sets the maximum tick rate (update rate) for the server.

WARNING: High values can cause unwanted behavior.
         
        MaxPlayers	MaxPlayers=70	Sets the maximum number of players.


         */
    }
}
