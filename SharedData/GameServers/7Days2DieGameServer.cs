﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SharedData
{
    public class _7Days2DieGameServer
    {
        public int? GameID { get; set; }
        public ulong UniqueGameServerID { get; set; }
        public string ServerName { get; set; }
        public string IPAddress { get; set; }

        public List<string> GetConfigFileLocations()
        {
            return new List<string>()
            {
                this.GetDirectoryPath() + Path.Combine("serverconfig.xml")
            };
        }
       
    }

    public static class GameServerExtensions
    {
        public static string GetDirectoryPath(this _7Days2DieGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        public static string GetDirectoryPath(this TeamFortress2GameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }
    }
}
