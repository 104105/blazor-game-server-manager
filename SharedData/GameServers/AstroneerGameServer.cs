﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedData
{
    public class AstroneerGameServer
    {
        public int? GameID { get; set; }
        public ulong UniqueGameServerID { get; set; }
        public string ServerName { get; set; }
        public string IPAddress { get; set; }
    }
}
