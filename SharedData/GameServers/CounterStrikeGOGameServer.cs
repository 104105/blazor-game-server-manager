﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace SharedData
{
    public class CounterStrikeGOGameServer
    {
        public int? GameID { get; set; }
        public ulong UniqueGameServerID { get; set; }
        public string ServerName { get; set; }
        public string RConPassword { get; set; }
        public int? MaxPlayers { get; set; }
        public bool isPrivateServer { get; set; }
        public string PrivateServerPassword { get; set; }
        public bool EnableCheats { get; set; }
        public bool isLocalServer { get; set; }
        public bool EnableTeamBalance { get; set; }
        public int? MaxTeamBalanceDifference { get; set; }
        public int GameType { get; set; }
        public int GameMode { get; set; }
        public string MapGroup { get; set; }
        public string StartingMap { get; set; }
        public string GSLT { get; set; }
        public string IPAddress { get; set; }
        public string AutoExec { get; set; }
        public string ServerConfig { get; set; }

        public string ServerTags { get; set; }
        public byte isServerPausable { get; set; }
        public byte AllowVotes { get; set; }
        public byte AllTalk { get; set; }
        public byte SimulateEntitiesEveryOtherTick { get; set; }
        public byte ForceServerPreload { get; set; }
        public byte KickPlayersWithMMCooldown { get; set; }
        public byte AllowOtherWorkshopMaps { get; set; }
        public byte HibernateServerWhenEmpty { get; set; }
        public bool EnableLogging { get; set; }
        public byte KeepLogsInOneFile { get; set; }
        public byte LogBans { get; set; }
        public byte LogEchos { get; set; }
        public byte LogServerInformation { get; set; }
        public byte FlushLogOnEachWrite { get; set; }
        public byte TickRate { get; set; }
        public string RemoteMissingFilesDownloadUrl { get; set; }
        public byte AllowFileDownloads { get; set; }
        public byte AllowFileUploads { get; set; }
        public byte KickUnpureClients { get; set; }
        public byte TracePureCheck { get; set; }
        public byte AutoKickPlayers { get; set; }
        public int SpawnProtectionTime { get; set; }
        public int DamageThresholdToKickPlayer { get; set; }
        public int DamageThresholdToWarnKickPlayer { get; set; }
        public int DamageThresholdToKickPlayerStartOfRound { get; set; }
     
        public void ParseConfigs()
        {
         AutoExec = 
$@"
/////////////////////////////////////////////////////////////////////////////////
// CUSTOM

mp_autokick ""{AutoKickPlayers}""                       // This command sets whether or not the auto kick feature should be enabled. The auto kick feature kicks people for being idle (AFK) and team damage. Default is 1 (enabled).
mp_spawnprotectiontime ""{SpawnProtectionTime}""                                  //If someone team kills within the amount of time set by this command after round start (i.e. if someone kills someone within 5 seconds of the round starting), they will be kicked from the server. You can disable this by setting the command to 0.
mp_td_dmgtokick ""{DamageThresholdToKickPlayer}""     //This command sets the amount of damage that a player has to do to their teammates in order to get kicked.
mp_td_dmgtowarn ""{DamageThresholdToWarnKickPlayer}"" // The damage threshhold players have to exceed in a match to get warned that they are about to be kicked.
mp_td_spawndmgthreshold ""{DamageThresholdToKickPlayerStartOfRound}"" // The damage threshold players have to exceed at the start of the round to be warned/kick.
maxplayers {MaxPlayers}
mp_limitteams {MaxTeamBalanceDifference}   //This command sets the maximum amount of different in players that there can be between two teams. If this was set to 1, if the CT team had 3 and T team had 2, a new player would not be able to join the CT team. Setting this to 0 disables the check. Default is 2.
mp_autoteambalance {EnableTeamBalance} //This command can be used to enable or disable the auto team balance feature. This feature will swap players around teams at the end of a round if there are an unequal amount of players on either time (i.e. if team A has 1 player and team B has 3, at the end of the round, 1 player from team B would be automatically moved over to team A).

hostname ""{ServerName}""
sv_tags ""{ServerTags}""

/////////////////////////////////////////////////////////////////////////////////
// Passwords

rcon_password ""{ RConPassword}""                                // Remote console password
sv_password ""{PrivateServerPassword}""                                    // Only set this if you intend to have a private server!

/////////////////////////////////////////////////////////////////////////////////
// Server Convars

host_name_store ""1""                                // Shows the server name
host_info_show ""2""                                // Shows the server information
host_players_show ""2""                            // Shows the player information
sv_pausable ""{isServerPausable}""                                    // Is the server pausable
sv_allow_votes ""{AllowVotes}""                              // Allow voting?
sv_alltalk ""{AllTalk}""                                    // Players can hear all other players' voice communication, no team restrictions
sv_alternateticks ""{SimulateEntitiesEveryOtherTick}""                            // If set, server only simulates entities on even numbered ticks
sv_cheats ""{EnableCheats}""                                    // Allow cheats on server
sv_forcepreload ""{ForceServerPreload}""                                // Force server side preloading
sv_kick_players_with_cooldown ""{KickPlayersWithMMCooldown}""                // Kicks players that are on a cooldown from Matchmaking
sv_maxusrcmdprocessticks ""0""                    // Fixes stuttering from rubberbanding
sv_workshop_allow_other_maps ""{AllowOtherWorkshopMaps}""                 // Users can play other workshop maps on this server
sv_hibernate_when_empty ""{HibernateServerWhenEmpty}""                        // Hibernates the server when no users are on
sv_lan ""{isLocalServer}"" //This should always be set, so you know it's not on


/////////////////////////////////////////////////////////////////////////////////
// Server Logging

log {(EnableLogging ? "on" : "off")}                                             // Enable or disable logging on the server
sv_log_onefile ""{KeepLogsInOneFile}""                                // Log server information to only one file
sv_logbans ""{LogBans}""                                    // Log server bans in the server logs
sv_logecho ""{LogEchos}""                                    // Echo log information to the console
sv_logfile ""{LogServerInformation}""                                    // Log server information in the log file
sv_logflush ""{FlushLogOnEachWrite}""                                    // Flush the log file to disk on each write (slow)
sv_logsdir ""logs""                                 // Folder in the game directory where server logs will be stored

/////////////////////////////////////////////////////////////////////////////////
// Server Rates (Set for 128Tick)

fps_max ""0""                                        // Sets the servers max fps
sv_minrate ""196608""                                // Min bandwidth rate allowed on server, 0 == unlimited
sv_maxrate ""0""                                    // Max bandwidth rate allowed on server, 0 == unlimited
sv_mincmdrate ""{TickRate}""                                // This sets the minimum value for cl_cmdrate. 0 == unlimited
sv_maxupdaterate ""{TickRate}""                            // Mmaximum updates per second that the server will allow
sv_minupdaterate ""{TickRate}""                            // Minimum updates per second that the server will allow



/////////////////////////////////////////////////////////////////////////////////
// Server Downloads

// sv_downloadurl ""{RemoteMissingFilesDownloadUrl}""
sv_allowdownload {AllowFileDownloads}
sv_allowupload {AllowFileUploads}

//////////////////////////////////////w///////////////////////////////////////////
// Pure Options

sv_pure_kick_clients ""{KickUnpureClients}""   // If set to 1, the server will kick clients with mismatching files
sv_pure_trace ""{TracePureCheck}""             // If set to 1, the server will print a message whenever a client is verifying a CRC for a file
";

            ServerConfig = $@"
/////////////////////////////////////////////////////////////////////////////////
// Ban Management

exec banned_user.cfg
exec banned_ip.cfg
writeid
writeip
";
        
        }
    }
}
