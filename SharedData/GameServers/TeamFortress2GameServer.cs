﻿using SharedData.Config;
using SharedData.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SharedData
{
    public class TeamFortress2GameServer
    {
        public int? GameID { get; set; }
        public ulong UniqueGameServerID { get; set; }
        public string ServerName { get; set; }
        public string IPAddress { get; set; }
        public static TeamFortress2Profile DefaultProfile;

        static TeamFortress2GameServer()
        {
            DefaultProfile = new TeamFortress2Profile();

            ConfigFile serverCfg = new ConfigFile()
            {
                Version = 1,
                FileRelativePath = Path.Combine("tf2", "cfg", "server.cfg"),
                DefaultText =
                "",
            };
         
        }

        public List<string> GetConfigFileLocations()
        {
            return new List<string>()
            {
                this.GetDirectoryPath() + Path.Combine("tf2" ,"cfg","server.cfg"),
                this.GetDirectoryPath()  + Path.Combine("tf2","cfg",""),
            };
        }
    }
}
