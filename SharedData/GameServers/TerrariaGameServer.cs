﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedData
{
    public class TerrariaGameServer
    {
        public string ServerName { get; set; }
        public string WorldPath { get; set; }
        public string WorldSavePath { get; set; } 
        public string DefaultConfigFile { get; set; }
        public string WorldName { get; set; }
        public string WorldSeed { get; set; } 
        public int WorldSize { get; set; } 
        public int Difficulty { get; set; } 
        public int? MaxPlayers { get; set; }
        public int? PortNumber { get; set; } 
        public string Password { get; set; } = string.Empty;
        public string MOTD { get; set; }
        public bool EnableCheatProtection { get; set; } = true;
        public int CPUPriority { get; set; } = 0;
        public int NPCSkipping { get; set; } = 60;
        public string IPAddress { get; set; }
        public bool EnableSteamSupport { get; set; }
        public bool AllowFriendsToJoin { get; set; }
        public bool DisableAccouncementBox { get; set; }
        public int? AnnouncementBoxRange { get; set; } 
        public string ServerLanguage { get; set; } = "en-US";
        public ulong UniqueGameServerID { get; set; }
        public int? GameID { get; set; }

       // public static string DataFolder { get { return "/Data/Terraria/"; }  }
        //public static string WorldFolder { get { return "/Data/Terraria/Worlds/"; } }
        public TerrariaGameServer()
        {
        }

        public void ParseConfigfile()
        {

            DefaultConfigFile =
            $@"
#this is an example config file for TerrariaServer.exe
#use the command 'TerrariaServer.exe -config serverconfig.txt' to use this configuration or run start-server.bat
#please report crashes by emailing crashlog.txt to support@terraria.org

#the following is a list of available command line parameters:

#-config <config file>				            Specifies the configuration file to use.
#-port <port number>				              Specifies the port to listen on.
#-players <number> / -maxplayers <number>	Sets the max number of players
#-pass <password> / -password <password>	Sets the server password
#-world <world file>				              Load a world and automatically start the server.
#-autocreate <#>			                  	Creates a world if none is found in the path specified by -world. World size is specified by: 1(small), 2(medium), and 3(large).
#-banlist <path>			                  	Specifies the location of the banlist. Defaults to banlist.txt in the working directory.
#-worldname <world name>             			Sets the name of the world when using -autocreate.
#-secure			                        		Adds addition cheat protection to the server.
#-noupnp				                        	Disables automatic port forwarding
#-steam                         					Enables Steam Support
#-lobby <friends> or <private>             Allows friends to join the server or sets it to private if Steam is enabled
#-ip <ip address>	Sets the IP address for the server to listen on
#-forcepriority <priority>	Sets the process priority for this task. If this is used the priority setting below will be ignored.
#-disableannouncementbox   Disables the text announcements Announcement Box makes when pulsed from wire.
#-announcementboxrange <number>   Sets the announcement box text messaging range in pixels, -1 for serverwide announcements.
#-seed <seed> Specifies the world seed when using -autocreate

#remove the # in front of commands to enable them.

#Load a world and automatically start the server. C:\Users\YOUR_USERNAME_HERE\My Documents\My Games\Terraria\Worlds\world1.wld
{(string.IsNullOrWhiteSpace(WorldPath) ? "#" : "")}world={WorldPath}

#Creates a new world if none is found. World size is specified by: 1(small), 2(medium), and 3(large).
autocreate={WorldSize}

#Sets the world seed when using autocreate
seed={WorldSeed}

#Sets the name of the world when using autocreate
worldname={WorldName}

#Sets the difficulty of the world when using autocreate 0(normal), 1(expert)
difficulty={Difficulty}

#Sets the max number of players allowed on a server.  Value must be between 1 and 255
maxplayers={MaxPlayers}

#Set the port number
port={PortNumber}

#Set the server password
password={Password}

#Set the message of the day
motd={MOTD}

#Sets the folder where world files will be stored
worldpath={WorldSavePath}

#The location of the banlist. Defaults to banlist.txt in the working directory.
#banlist=banlist.txt

#Adds addition cheat protection.
secure={(EnableCheatProtection ? 1 : 0)}

#Sets the server language from its language code. 
#English = en-US, German = de-DE, Italian = it-IT, French = fr-FR, Spanish = es-ES, Russian = ru-RU, Chinese = zh-Hans, Portuguese = pt-BR, Polish = pl-PL,
language={ServerLanguage}

#Automatically forward ports with uPNP
upnp=1

#Reduces enemy skipping but increases bandwidth usage. The lower the number the less skipping will happen, but more data is sent. 0 is off.
npcstream={NPCSkipping}

#Default system priority 0:Realtime, 1:High, 2:AboveNormal, 3:Normal, 4:BelowNormal, 5:Idle
priority = {CPUPriority}";
        }
    }
}
