﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SharedData
{
    public class GameServerIDWithGameID
    {
        public ulong GameServerID { get; set; }
        public int GameID { get; set; }
    }
}
