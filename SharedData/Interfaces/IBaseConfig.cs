﻿using System;
using System.Collections.Generic;
using System.Text;
using SharedData;
namespace SharedData.Interfaces
{
    interface IBaseConfig
    {
        public GameID GameID { get; set; }
        public string ProfileName { get; set; }
        List<ConfigFile> Files { get;set; }
        public void SaveConfig();
        public void GetConfig(string FileName);

    }

    public struct ConfigFile
    {
        public string DefaultText { get; set; }
        public string Text { get; set; }
        public float Version { get; set; }
        public string FileRelativePath { get; set; }
    }
}
