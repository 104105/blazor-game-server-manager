﻿using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.IO;
using System.Collections.Generic;
//File Manager's base functions are available in the below namespace
using Syncfusion.EJ2.FileManager.Base;
using Syncfusion.EJ2.FileManager.PhysicalFileProvider;
using Daemon;
using SharedData;
using Daemon.Internal.Games;
//File Manager's operations are available in the below namespace


[Route("api/v1/FileManagerProvider/")]
    [ApiController]
    public class FTPProviderController : Controller
    {
        public PhysicalFileProvider operation;
        public string basePath;

    public FTPProviderController(IHostingEnvironment hostingEnvironment)
    {
        this.basePath = hostingEnvironment.ContentRootPath;
        this.operation = new PhysicalFileProvider();
        this.operation.RootFolder(this.basePath ); // Data\\Files denotes in which files and folders are available.
    }

    [HttpPost("ViewGameServerFolder")]
    public IActionResult ViewGameServerFolder(GameServerIDWithGameID arg)
    {
        GameID gameId = (GameID)arg.GameID;

        switch (gameId)
        {
            case GameID.CounterStrikeGO:
                foreach (var item in GlobalVariables.gSettings.gCounterStrikeGoGameServers)
                {
                    if (item.UniqueGameServerID == arg.GameServerID)
                    {
                        Console.WriteLine("set gameserver root " + CSGOManager.GetDirectoryPath(item));
                        operation.RootFolder(CSGOManager.GetDirectoryPath(item));
                        return Ok();
                    }
                }
                break;
            case GameID.Terraria:
                break;
            default:
                break;
        }
        return NotFound();        
    }


    // Processing the File Manager operations
    [Route("FileOperations")]
        public object FileOperations([FromBody] FileManagerDirectoryContent args)
        {
            switch (args.Action)
            {
                // Add your custom action here
                case "read":
                    // Path - Current path; ShowHiddenItems - Boolean value to show/hide hidden items
                    return this.operation.ToCamelCase(this.operation.GetFiles(args.Path, args.ShowHiddenItems));
                case "delete":
                    // Path - Current path where of the folder to be deleted; Names - Name of the files to be deleted
                    return this.operation.ToCamelCase(this.operation.Delete(args.Path, args.Names));
                case "copy":
                    //  Path - Path from where the file was copied; TargetPath - Path where the file/folder is to be copied; RenameFiles - Files with same name in the copied location that is confirmed for renaming; TargetData - Data of the copied file
                    return this.operation.ToCamelCase(this.operation.Copy(args.Path, args.TargetPath, args.Names, args.RenameFiles, args.TargetData));
                case "move":
                    // Path - Path from where the file was cut; TargetPath - Path where the file/folder is to be moved; RenameFiles - Files with same name in the moved location that is confirmed for renaming; TargetData - Data of the moved file
                    return this.operation.ToCamelCase(this.operation.Move(args.Path, args.TargetPath, args.Names, args.RenameFiles, args.TargetData));
                case "details":
                    // Path - Current path where details of file/folder is requested; Name - Names of the requested folders
                    return this.operation.ToCamelCase(this.operation.Details(args.Path, args.Names));
                case "create":
                    // Path - Current path where the folder is to be created; Name - Name of the new folder
                    return this.operation.ToCamelCase(this.operation.Create(args.Path, args.Name));
                case "search":
                    // Path - Current path where the search is performed; SearchString - String typed in the searchbox; CaseSensitive - Boolean value which specifies whether the search must be casesensitive
                    return this.operation.ToCamelCase(this.operation.Search(args.Path, args.SearchString, args.ShowHiddenItems, args.CaseSensitive));
                case "rename":
                    // Path - Current path of the renamed file; Name - Old file name; NewName - New file name
                    return this.operation.ToCamelCase(this.operation.Rename(args.Path, args.Name, args.NewName));
            }
            return null;
        }

    // Processing the Download operation
    [Route("Download")]
    public IActionResult Download([FromForm] string downloadInput)
    {
        Console.WriteLine(downloadInput + "dl");
        Console.WriteLine("dl");
        //Invoking download operation with the required paramaters
        // path - Current path where the file is downloaded; Names - Files to be downloaded;
        FileManagerDirectoryContent args = JsonConvert.DeserializeObject<FileManagerDirectoryContent>(downloadInput);
        return operation.Download(args.Path, args.Names);
    }





    [Route("Upload")]
    public IActionResult Upload([FromForm] string path, [FromForm] IList<IFormFile> uploadFiles, [FromForm] string action)
    {


        Console.WriteLine("UPLOAD");
        //Invoking upload operation with the required paramaters
        // path - Current path where the file is to uploaded; uploadFiles - Files to be uploaded; action - name of the operation(upload)
        FileManagerResponse uploadResponse;
        uploadResponse = operation.Upload(path, uploadFiles, action, null);
        if (uploadResponse.Error != null)
        {
            Console.WriteLine(uploadResponse.Error.Message);
            Response.Clear();
            Response.ContentType = "application/json; charset=utf-8";
            Response.StatusCode = Convert.ToInt32(uploadResponse.Error.Code);
            Response.HttpContext.Features.Get<IHttpResponseFeature>().ReasonPhrase = uploadResponse.Error.Message;
        }
        return Content("");
    }

    [Route("GetImage")]
    public IActionResult GetImage(FileManagerDirectoryContent args)
    {
        //Invoking GetImage operation with the required paramaters
        // path - Current path of the image file; Id - Image file id;
        return this.operation.GetImage(args.Path, args.Id, false, null, null);
    }

}
