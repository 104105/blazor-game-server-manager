﻿namespace Daemon.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Management;
    using System.Net;
    using System.Threading.Tasks;
    using Daemon.Internal;
    using Daemon.Internal.Games;
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using SharedData;

    [Route("api/v1/DedicatedServer/")]
    [ApiController]
    public class DedicatedServerController : ControllerBase
    {
        [HttpGet("GetBasicInfo")]
        public IActionResult GetBasicInfo()
        {
            string cpu = string.Empty;
            ulong ramSize = 0;

            if (GlobalVariables.gSettings.isWindows)
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher(
            "select * from " + "Win32_Processor");

                foreach (var item in searcher.Get())
                {
                    cpu = (string)item.GetPropertyValue("Name");
                }
            }
            else
            {
                cpu = Helpers.LinuxTerminalCmd("cat /proc/cpuinfo | grep -m 1 \"model name\"").Replace("model name", string.Empty).Replace(" ",string.Empty);
            }

            DriveInfo[] allDrives = DriveInfo.GetDrives();
            List<HardDrive> hDrive = new List<HardDrive>();

            foreach (DriveInfo d in allDrives)
            {
                 if (d.IsReady == true)
                {
                    hDrive.Add(new HardDrive() { Label = d.VolumeLabel, TotalFreeSpaceMB = (ulong)d.TotalFreeSpace / 1000000, TotalSizeMB = (ulong)d.TotalSize / 1000000 });
                }
            }

            if (GlobalVariables.gSettings.isWindows)
            {
                ManagementObjectSearcher
            searcher = new ManagementObjectSearcher(
         "select * from " + "Win32_PhysicalMemory");

                foreach (var item in searcher.Get())
                {
                    foreach (PropertyData pd in item.Properties)
                    {
                        if (pd.Name.Equals("Capacity"))
                        {
                            ramSize += (ulong)pd.Value;
                        }
                    }
                }
            }
            else
            {
               Console.WriteLine( Helpers.LinuxTerminalCmd("cat /proc/meminfo | grep \"MemTotal\"").Replace("MemTotal:", string.Empty).Replace("kB", string.Empty).Replace(" ", string.Empty));
               ramSize = ulong.Parse(Helpers.LinuxTerminalCmd("cat /proc/meminfo | grep \"MemTotal\"").Replace("MemTotal:", string.Empty).Replace("kB", string.Empty).Replace(" ", string.Empty));
            }

            float cpuFrequency = 0;
            if (GlobalVariables.gSettings.isWindows)
            {
                ManagementObjectSearcher
            searcher = new ManagementObjectSearcher(
          "select MaxClockSpeed from Win32_Processor");
                foreach (var item in searcher.Get())
                {
                    cpuFrequency = (float)((uint)item["MaxClockSpeed"] * 0.001f);
                }
            }
            else
            {
                cpuFrequency = float.Parse(Helpers.LinuxTerminalCmd("cat /proc/cpuinfo | grep -m 1 \"cpu MHz\"").Replace("cpu MHz", string.Empty).Replace(":", string.Empty).Replace(" ", string.Empty)) * 0.001f;
            }

            var basicInfo = new BasicInfo();
            basicInfo.CPU = cpu;
            basicInfo.MemoryMB = ramSize;
            basicInfo.HardDrives = hDrive;
            basicInfo.CPUCores = Environment.ProcessorCount;
            basicInfo.CPUFrequency = cpuFrequency;

            string strHostName = Dns.GetHostName();

            // Find host by name
            IPHostEntry iphostentry = Dns.GetHostEntry(strHostName);

            basicInfo.IPAddress = new List<string>();
            // Enumerate IP addresses
            foreach (IPAddress ipaddress in iphostentry.AddressList)
            {
                basicInfo.IPAddress.Add(ipaddress.ToString());
            }


            return this.Ok(basicInfo);
        }

        [HttpGet("GetSyncInfo")]
        public IActionResult GetSyncInfo()
        {
            SyncInfo syncInfo = new SyncInfo()
            {
            };

            if (GlobalVariables.gDedidicatedStas.Count > 301)
            {
                syncInfo.DediStats = GlobalVariables.gDedidicatedStas.GetRange(GlobalVariables.gDedidicatedStas.Count - 301, 300);

            }else
            {
                syncInfo.DediStats = GlobalVariables.gDedidicatedStas.GetRange(0, GlobalVariables.gDedidicatedStas.Count);

            }


            return Ok(syncInfo);
        }

        [HttpGet("GetLatestDedicatedStat")]
        public IActionResult GetLatestDediStat()
        {
            return Ok(GlobalVariables.gDedidicatedStas.Last());
        }

        [HttpPost("CreateNewCounterStrikeGOGameServer")]
        public IActionResult CreateCSGOGameServer([FromBody]CounterStrikeGOGameServer data)
        {
            return this.Ok(CSGOManager.InstallCSGoServer(data));
        }

        [HttpPost("CreateNewTerrariaGameServer")]
        public IActionResult CreateTerrariaGameServer([FromBody] TerrariaGameServer data)
        {
            return this.Ok(TerrariaManager.InstallTerrariaServer(data));
        }

        [HttpPost("CreateNewConanExilesGameServer")]
        public IActionResult CreateConanExilesGameServer([FromBody] ConanExilesGameServer data)
        {
            return this.Ok(ConanExilesManager.InstallConanExilesGameServer(data));
        }

        [HttpPost("CreateNew7DaysToDieGameServer")]
        public IActionResult CreateNew7DaysToDieGameServer([FromBody] _7Days2DieGameServer data)
        {
            return this.Ok(SevenDays2DieManager.Install7Days2DieServer(data));
        }

        [HttpPost("CreateNewArkSEGameServer")]
        public IActionResult CreateNewArkSEGameServer([FromBody] ArkGameServer data)
        {
            return this.Ok(ArkSurvivalEvolvedManager.InstallArkSEServer(data));
        }

        [HttpPost("CreateNewTF2GameServer")]
        public IActionResult CreateNewTF2GameServer([FromBody] TeamFortress2GameServer data)
        {
            return this.Ok(TF2Manager.InstallTF2Server(data));
        }

        [HttpPost("CreateNewGarrysModServer")]
        public IActionResult CreateNewGarrysModServer([FromBody] GarrysModGameServer data)
        {
            return this.Ok(GarrysModManager.InstallGarrysModServer(data));
        }

        [HttpPost("CreateNewRustServer")]
        public IActionResult CreateNewRustServer([FromBody] RustGameServer data)
        {
            return this.Ok(RustManager.InstallRustServer(data));
        }


        [HttpPost("CreateNewKillingFloor2Server")]
        public IActionResult CreateNewKillingFloor2Server([FromBody] KillingFloor2GameServer data)
        {
            return this.Ok(KillingFloor2Manager.InstalKillingFloor2Server(data));
        }

        [HttpPost("CreateNewTheForestServer")]
        public IActionResult CreateNewTheForestServer([FromBody] TheForestGameServer data)
        {
            return this.Ok(TheForestManager.InstalTheForestServer(data));
        }

        [HttpPost("CreateProjectZomboidServer")]
        public IActionResult CreateProjectZomboidServer([FromBody] ProjectZomboidGameServer data)
        {
            return this.Ok(ProjectZomboidManager.InstallProjectZomboidServer(data));
        }

        [HttpPost("CreateLeft4Dead2Server")]
        public IActionResult CreateLeft4Dead2Server([FromBody] Left4Dead2GameServer data)
        {
            return this.Ok(Left4Dead2Manager.InstallGameServer(data));
        }

        [HttpPost("CreateSquadServer")]
        public IActionResult CreateSquadServer([FromBody] SquadGameServer data)
        {
            return this.Ok(SquadManager.InstallGameServer(data));
        }
        [HttpPost("CreateSpaceEngineersServer")]
        public IActionResult CreateSpaceEngineersServer([FromBody] SpaceEngineersGameServer data)
        {
            return this.Ok(SpaceEngineersManager.InstallGameServer(data));
        }


        [HttpPost("CreateHellLetLooseServer")]
        public IActionResult CreateHellLetLooseServer([FromBody] HellLetLooseGameServer data)
        {
            return this.Ok(HellLetLooseManager.InstallGameServer(data));
        }
        [HttpPost("CreateSCPSecretLaboratoryServer")]
        public IActionResult CreateSCPSecretLaboratoryServer([FromBody] SCPSecretLaboratoryGameServer data)
        {
            return this.Ok(SCPSecretLaboratoryManager.InstallGameServer(data));
        }

        [HttpPost("CreateMordhauServer")]
        public IActionResult CreateMordhauServer([FromBody] MordhauGameServer data)
        {
            return this.Ok(MordhauManager.InstallGameServer(data));
        }
        [HttpPost("CreateTheIsleServer")]
        public IActionResult CreateTheIsleServer([FromBody] TheIsleGameServer data)
        {
            return this.Ok(TheIsleManager.InstallGameServer(data));
        }

        [HttpPost("CreateEmpyrionGalacticSurvivalServer")]
        public IActionResult CreateEmpyrionGalacticSurvivalManagerServer([FromBody] EmpyrionGalacticSurvivalGameServer data)
        {
            return this.Ok(EmpyrionGalacticSurvivalManager.InstallGameServer(data));
        }

        [HttpPost("CreateAstroneerServer")]
        public IActionResult CreateAstroneerServer([FromBody] AstroneerGameServer data)
        {
            return this.Ok(AstroneerManager.InstallGameServer(data));
        }

        [HttpPost("CreateCounterStrikeServer")]
        public IActionResult CreateCounterStrikeServer([FromBody] CounterStrikeGameServer data)
        {
            return this.Ok(CounterStrikeManager.InstallGameServer(data));
        }

        [HttpPost("CreateCounterStrikeSourceServer")]
        public IActionResult CreateCounterStrikeSourceServer([FromBody] CounterStrikeSourceGameServer data)
        {
            return this.Ok(CounterStrikeSourceManager.InstallGameServer(data));
        }

        [HttpPost("CreateCounterStrikeConditionZeroServer")]
        public IActionResult CreateCounterStrikeConditionZeroServer([FromBody] CounterStrikeConditionZeroGameServer data)
        {
            return this.Ok(CounterStrikeConditionZeroManager.InstallGameServer(data));
        }


        [HttpPost("CreateFake")]
        public IActionResult CreateFake()
        {
          //  InternalManager.InstallFake();
            return this.Ok();
        }

    }
}