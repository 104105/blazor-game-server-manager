﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Daemon.Internal;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Diagnostics;
using SharedData;

namespace Daemon.Controllers
{
    [Route("api/v1/Console/")]
    [ApiController]
    public class ConsoleController : ControllerBase
    {

        [HttpPost("GetConsoleInfo")]
        public IActionResult GetConsoleInfo([FromBody] ulong serverID)
        {
            Console.WriteLine(serverID);
            return Ok(ProcessIOManager.GetLines(serverID));
        }

        [HttpPost("GetAllConsoleInfo")]
        public IActionResult GetAllConsoleInfo([FromBody] ulong serverID)
        {
            Console.WriteLine(serverID);
            return Ok(ProcessIOManager.GetAllLines(serverID));
        } 

        [HttpPost("SendConsoleCommand")]
        public IActionResult SendConsoleCommand(ConsoleCommand cmd)
        {
            int? pID = InternalManager.GameServerIDToProcessID(cmd.GameServerID);

            if (pID != null)
            {
                foreach (var item in GlobalVariables.gRunningProcesses)
                {
                    if(item.Id == pID)
                    {

                        InternalManager.EnterConsoleCommand(item, cmd.Command);
                        return Ok();
                    }
                }
            }

            Console.WriteLine("Server Not Found, Unique ID:  " + cmd.GameServerID);

            return NotFound();
        }

        

    }
}