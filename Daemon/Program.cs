using System;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Runtime.InteropServices;
using System.Threading;
using Daemon.Internal;
using Daemon.Internal.Games;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using SharedData;
using WindowsFirewallHelper;

namespace Daemon
{
    public class Program
    {
        public static void Main(string[] args)
        {
            bool isSucessfull = InitializeDaemon();
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });

        public static bool InitializeDaemon()
        {
            GlobalVariables.gWebClient = new System.Net.WebClient();
            if (!File.Exists(GlobalVariables.gSettingsFile))
            {
                try
                {     //new installation
                    GlobalVariables.gSettings = new Models.Local.SettingsModel();

                    GlobalVariables.gSettings.Init();
                    GlobalVariables.gSettings.isWindows = RuntimeInformation.IsOSPlatform(OSPlatform.Windows);
                    GlobalVariables.gSettings.isLinux = RuntimeInformation.IsOSPlatform(OSPlatform.Linux);
                    //new setup
                    if (!Directory.Exists("Cache"))
                    {
                        Directory.CreateDirectory(Path.Combine(".", "Cache", "Games", "Terraria"));
                        Directory.CreateDirectory(Path.Combine(".", "Cache", "Games"));
                        Directory.CreateDirectory(Path.Combine(".", "Games"));
                        Directory.CreateDirectory(Path.Combine(".", "Stats"));
                        Directory.CreateDirectory(GlobalVariables.gTerrariaWorldSavePath);
                    }

                    var rule = FirewallManager.Instance.CreateApplicationRule(FirewallProfiles.Domain | FirewallProfiles.Private | FirewallProfiles.Public, @"Universal Game Server Manager", FirewallAction.Allow, Process.GetCurrentProcess().MainModule.FileName);
                    Console.Write(Process.GetCurrentProcess().MainModule.FileName);
                    rule.Direction = FirewallDirection.Outbound | FirewallDirection.Inbound;

                    if (!FirewallManager.Instance.Rules.Contains(rule))
                    {
                        FirewallManager.Instance.Rules.Add(rule);
                    }

                    GlobalVariables.SaveSettingsFile();
                    InstallSteamCMD();
                    UpdateSteamCMD();
                }catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
            else
            {
               //Load existing Settings file
                GlobalVariables.gSettings = GlobalVariables.LoadSettingsFile();
               
                //Start game servers
                foreach (var item in GlobalVariables.gSettings.gTerrariaGameServers) 
                {
                    TerrariaManager.StartTerrariaServer(item);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gCounterStrikeGoGameServers)
                {
                    CSGOManager.StartCSGoServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gConanExilesGameServers)
                {
                    ConanExilesManager.StartConanExilesGameServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.g7Days2DieGameServers)
                {
                    SevenDays2DieManager.Start7Days2DieServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gArkGameServers)
                {
                    ArkSurvivalEvolvedManager.StartArkSEServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gTF2GameServers)
                {
                    TF2Manager.StartTF2Server(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gGarrysModGameServers)
                {
                    GarrysModManager.StartGarrysModServer(gameServer);
                }
                foreach (var gameServer in GlobalVariables.gSettings.gRustGameServers)
                {
                    RustManager.StartRustServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gKillingFloor2GameServers)
                {
                    KillingFloor2Manager.StartKillingFloor2Server(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gTheForestGameServers)
                {
                    TheForestManager.StartTheForestServer(gameServer);
                }


                foreach (var gameServer in GlobalVariables.gSettings.gProjectZomboidGameServers)
                {
                    ProjectZomboidManager.StartProjectZomboidServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gLeft4Dead2GameServers)
                {
                    Left4Dead2Manager.StartGameServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gSquadGameServers)
                {
                    SquadManager.StartGameServer(gameServer);
                }


                foreach (var gameServer in GlobalVariables.gSettings.gSpaceEngineersGameServers)
                {
                    SpaceEngineersManager.StartGameServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gHellLetLooseGameServers)
                {
                    HellLetLooseManager.StartGameServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gSCPSecretLaboratoryGameServers)
                {
                    SCPSecretLaboratoryManager.StartGameServer(gameServer);
                }


                foreach (var gameServer in GlobalVariables.gSettings.gMordhauGameServers)
                {
                    MordhauManager.StartGameServer(gameServer);
                }


                foreach (var gameServer in GlobalVariables.gSettings.gTheIsleGameServers)
                {
                    TheIsleManager.StartGameServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gEmpyrionGalacticSurvivalGameServers)
                {
                    EmpyrionGalacticSurvivalManager.StartGameServer(gameServer);
                }
                foreach (var gameServer in GlobalVariables.gSettings.gAstroneerGameServers)
                {
                    AstroneerManager.StartGameServer(gameServer);
                }
                foreach (var gameServer in GlobalVariables.gSettings.gCounterStrikeGameServers)
                {
                    CounterStrikeManager.StartGameServer(gameServer);
                }
                foreach (var gameServer in GlobalVariables.gSettings.gCounterStrikeSourceGameServers)
                {
                    CounterStrikeSourceManager.StartGameServer(gameServer);
                }

                foreach (var gameServer in GlobalVariables.gSettings.gCounterStrikeConditionZeroGameServers)
                {
                    CounterStrikeConditionZeroManager.StartGameServer(gameServer);
                }
            }
            

            //Setup timed events
            System.Timers.Timer statManager = new System.Timers.Timer(5000);

            //Poll server stats
            statManager.Elapsed += StatManager.PollDedicatedStats;
            Console.WriteLine("RUN 1");
            //Check if Game Servers crashed
            statManager.Elapsed += ProcessCrashManager.CheckIfGameServersCrashed;


            statManager.AutoReset = true;
            statManager.Start();

            AppDomain.CurrentDomain.ProcessExit += CurrentDomain_ProcessExit;

            return true;
        }

        private static void CurrentDomain_ProcessExit(object sender, EventArgs e)
        {
            Console.WriteLine("code EXIT EVENT");
        }

        private static void InstallSteamCMD()
        {
            if (GlobalVariables.gSettings.isWindows)
            {
                if (!File.Exists(GlobalVariables.gCachedSteamCMDZipFile))
                {
                    GlobalVariables.gWebClient.DownloadFile(GlobalVariables.gSteamCMDDownloadLink, GlobalVariables.gCachedSteamCMDZipFile);
                }
                ZipFile.ExtractToDirectory(GlobalVariables.gCachedSteamCMDZipFile, Path.GetFullPath(Path.Combine(".", "SteamCMD")), true);
            }
            else
            {
                Helpers.LinuxTerminalCmd("sudo apt-get install steamcmd");
            }
        }

        static void UpdateSteamCMD()
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo(GlobalVariables.gSteamCMDExePath, "+quit");

            Process proc = Process.Start(processStartInfo);

        }
    }
}
