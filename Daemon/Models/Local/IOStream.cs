﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Daemon.Models.Local
{
    public class IOStream
    {
        public List<string> AllLines = new List<string>();
        public List<string> UnSyncedLines = new List<string>();
    }
}
