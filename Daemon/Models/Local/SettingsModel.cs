﻿using Newtonsoft.Json;
using SharedData;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Models.Local
{
    public class SettingsModel
    {
        public bool isWindows { get; set; }
        public bool isLinux { get; set; }
        public ulong m_CurrentUniqueGameServerID
        { get;  set; }
        public List<TerrariaGameServer> gTerrariaGameServers { get; set; }
        public List<CounterStrikeGOGameServer> gCounterStrikeGoGameServers { get; set; }
        public List<ConanExilesGameServer> gConanExilesGameServers { get; set; }
        public List<_7Days2DieGameServer> g7Days2DieGameServers { get; set; }
        public List<ArkGameServer> gArkGameServers { get; set; }
        public List<TeamFortress2GameServer> gTF2GameServers { get; set; }
        public List<GarrysModGameServer> gGarrysModGameServers { get; set; }
        public List<RustGameServer> gRustGameServers { get; set; }
        public List<KillingFloor2GameServer> gKillingFloor2GameServers { get; set; }
        public List<TheForestGameServer> gTheForestGameServers { get; set; }
        public List<ProjectZomboidGameServer> gProjectZomboidGameServers { get; set; }
        public List<Left4Dead2GameServer> gLeft4Dead2GameServers { get; set; }
        public List<SquadGameServer> gSquadGameServers { get; set; }
        public List<SpaceEngineersGameServer> gSpaceEngineersGameServers { get; set; }
        public List<HellLetLooseGameServer> gHellLetLooseGameServers { get; set; }
        public List<SCPSecretLaboratoryGameServer> gSCPSecretLaboratoryGameServers { get; set; }
        public List<MordhauGameServer> gMordhauGameServers { get; set; }
        public List<TheIsleGameServer> gTheIsleGameServers { get; set; }
        public List<EmpyrionGalacticSurvivalGameServer> gEmpyrionGalacticSurvivalGameServers { get; set; }
        public List<AstroneerGameServer> gAstroneerGameServers { get; set; }
        public List<CounterStrikeGameServer> gCounterStrikeGameServers { get; set; }
        public List<CounterStrikeSourceGameServer> gCounterStrikeSourceGameServers { get; set; }
        public List<CounterStrikeConditionZeroGameServer> gCounterStrikeConditionZeroGameServers { get; set; }

        public long gLastTotalBytesDownloaded { get; set; }
        public long gLastTotalBytesUploaded { get; set; }

        public bool Init()
        {
            gTerrariaGameServers = new List<TerrariaGameServer>();
            gCounterStrikeGoGameServers = new List<CounterStrikeGOGameServer>();
            gConanExilesGameServers = new List<ConanExilesGameServer>();
            g7Days2DieGameServers = new List<_7Days2DieGameServer>();
            gArkGameServers = new List<ArkGameServer>();
            gTF2GameServers = new List<TeamFortress2GameServer>();
            gGarrysModGameServers = new List<GarrysModGameServer>();
            gRustGameServers = new List<RustGameServer>();
            gKillingFloor2GameServers = new List<KillingFloor2GameServer>();
            gTheForestGameServers = new List<TheForestGameServer>();
            gProjectZomboidGameServers = new List<ProjectZomboidGameServer>();
            gLeft4Dead2GameServers = new List<Left4Dead2GameServer>();
            gSquadGameServers = new List<SquadGameServer>();
            gHellLetLooseGameServers = new List<HellLetLooseGameServer>();
            gSCPSecretLaboratoryGameServers = new List<SCPSecretLaboratoryGameServer>();
            gMordhauGameServers = new List<MordhauGameServer>();
            gTheIsleGameServers = new List<TheIsleGameServer>();
            gEmpyrionGalacticSurvivalGameServers = new List<EmpyrionGalacticSurvivalGameServer>();
            gAstroneerGameServers = new List<AstroneerGameServer>();
            gCounterStrikeGameServers = new List<CounterStrikeGameServer>();
            gCounterStrikeSourceGameServers = new List<CounterStrikeSourceGameServer>();
            gCounterStrikeConditionZeroGameServers = new List<CounterStrikeConditionZeroGameServer>();
            gSpaceEngineersGameServers = new List<SpaceEngineersGameServer>();

            m_CurrentUniqueGameServerID = 2;

            System.Net.NetworkInformation.NetworkInterface[] nics;
            nics = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();
            gLastTotalBytesDownloaded = 0;
            gLastTotalBytesUploaded = 0;

            foreach (var nic in nics)
            {
                gLastTotalBytesDownloaded += nic.GetIPv4Statistics().BytesReceived;
                gLastTotalBytesUploaded += nic.GetIPv4Statistics().BytesSent;
            }


          
            return true;
        }

        public ulong GetUniqueGameServerID()
        {
            return m_CurrentUniqueGameServerID++;
        }

    }
}
