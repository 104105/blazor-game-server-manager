﻿using Daemon.Models.Local;
using Newtonsoft.Json;
using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace Daemon
{
    public static class GlobalVariables
    {
        public static string gSetupFile
        {
            get
            {
                return Path.Combine(".", "Setup.json");

            }
        }
        public static string gSettingsFile
        {
            get
            {
                return Path.Combine(".", "Settings.json");
            }
        }
        public static string gCachedSteamCMDZipFile
        {
            get
            {
                return Path.GetFullPath(Path.Combine(".", "Cache", "SteamCMD.zip"));
            }
        }
        public static string gSteamCMDDownloadLink
        {
            get
            {
                return "https://steamcdn-a.akamaihd.net/client/installer/steamcmd.zip";
            }
        }

        public static string gSteamCMDExePath
        {
            get
            {
                if (gSettings.isWindows) { return Path.GetFullPath(Path.Combine(".", "SteamCMD", "steamcmd.exe")); }
                else { return Path.Combine(Path.DirectorySeparatorChar.ToString(), "usr", "games", "steamcmd"); }
            }
        }

        public static string gTerrariaWorldSavePath
        {
            get
            {
                return Path.GetFullPath(Path.Combine(".", "Data", "Terraria"));
            }
        }



        public static List<Stastistic> gDedidicatedStas = new List<Stastistic>(17280);

        public static WebClient gWebClient;
        public static SettingsModel gSettings { get; set; }

        public static List<Process> gRunningProcesses = new List<Process>();
        public static string gCounterStrikeGOBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "CounterStrikeGlobalOffensive")) + Path.DirectorySeparatorChar; } }
        public static string gCounterStrikeGOGameServerDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "CounterStrikeGlobalOffensive") + Path.DirectorySeparatorChar); } }
        public static string gTerrariaBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "Terraria")) + Path.DirectorySeparatorChar; } }
        public static string gTerrariaGameServerDirectory { get { return Path.Combine(".", "Games", "Terraria") + Path.DirectorySeparatorChar; } }
        public static string gConanExilesBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "ConanExiles")) + Path.DirectorySeparatorChar; } }
        public static string gConanExilesGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "ConanExiles") + Path.DirectorySeparatorChar); } }
        public static string g7Days2DieBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "SevenDays2Die")) + Path.DirectorySeparatorChar; } }
        public static string g7Days2DieGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "SevenDays2Die") + Path.DirectorySeparatorChar); } }
        public static string gArkSurvivalEvolvedBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "ArkSurvivalEvolved")) + Path.DirectorySeparatorChar; } }
        public static string gArkSurvivalEvolvedGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "ArkSurvivalEvolved") + Path.DirectorySeparatorChar); } }
        public static string gTF2BaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "TeamFortress2")) + Path.DirectorySeparatorChar; } }
        public static string gTF2GameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "TeamFortress2") + Path.DirectorySeparatorChar); } }
        public static string gGarrysModBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "GarrysMod")) + Path.DirectorySeparatorChar; } }
        public static string gGarrysGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "GarrysMod") + Path.DirectorySeparatorChar); } }
        public static string gRustBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "Rust")) + Path.DirectorySeparatorChar; } }
        public static string gRustGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "Rust") + Path.DirectorySeparatorChar); } }

        public static string gKillingFloor2BaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "KillingFloor2")) + Path.DirectorySeparatorChar; } }
        public static string gKillingFloor2GameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "KillingFloor2") + Path.DirectorySeparatorChar); } }
        public static string gTheForestBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "TheForest")) + Path.DirectorySeparatorChar; } }
        public static string gTheForestGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "TheForest") + Path.DirectorySeparatorChar); } }
        public static string gProjectZomboidBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "ProjectZomboid")) + Path.DirectorySeparatorChar; } }
        public static string gProjectZomboidGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "ProjectZomboid") + Path.DirectorySeparatorChar); } }
        public static string gLeft4Dead2BaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "Left4Dead2")) + Path.DirectorySeparatorChar; } }
        public static string gLeft4Dead2GameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "Left4Dead2") + Path.DirectorySeparatorChar); } }
        public static string gSquadBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "Squad")) + Path.DirectorySeparatorChar; } }
        public static string gSquadGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "Squad") + Path.DirectorySeparatorChar); } }
        public static string gSpaceEngineersBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "SpaceEngineers")) + Path.DirectorySeparatorChar; } }
        public static string gSpaceEngineersGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "SpaceEngineers") + Path.DirectorySeparatorChar); } }
        public static string gHellLetLooseBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "HellLetLoose")) + Path.DirectorySeparatorChar; } }
        public static string gHellLetLooseGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "HellLetLoose") + Path.DirectorySeparatorChar); } }

        public static string gSCPSecretLaboratoryBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "SCPSecretLaboratory")) + Path.DirectorySeparatorChar; } }
        public static string gSCPSecretLaboratoryGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "SCPSecretLaboratory") + Path.DirectorySeparatorChar); } }
        public static string gMordhauBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "Mordhau")) + Path.DirectorySeparatorChar; } }
        public static string gMordhauGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "Mordhau") + Path.DirectorySeparatorChar); } }
        public static string gTheIsleBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "TheIsle")) + Path.DirectorySeparatorChar; } }
        public static string gTheIsleGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "TheIsle") + Path.DirectorySeparatorChar); } }
        public static string gEmpyrionGalacticSurvivalBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "EmpyrionGalacticSurvival")) + Path.DirectorySeparatorChar; } }
        public static string gEmpyrionGalacticSurvivalGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "EmpyrionGalacticSurvival") + Path.DirectorySeparatorChar); } }
        public static string gAstroneerBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "Astroneer")) + Path.DirectorySeparatorChar; } }
        public static string gAstroneerGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "Astroneer") + Path.DirectorySeparatorChar); } }
        public static string gCounterStrikeBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "CounterStrike")) + Path.DirectorySeparatorChar; } }
        public static string gCounterStrikeGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "CounterStrike") + Path.DirectorySeparatorChar); } }
        public static string gCounterStrikeSourceBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "CounterStrikeSource")) + Path.DirectorySeparatorChar; } }
        public static string gCounterStrikeSourceGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "CounterStrikeSource") + Path.DirectorySeparatorChar); } }
        public static string gCounterStrikeConditionZeroBaseDirectory { get { return Path.GetFullPath(Path.Combine(".", "Cache", "Games", "CounterStrikeConditionZero")) + Path.DirectorySeparatorChar; } }
        public static string gCounterStrikeConditionZeroGameDirectory { get { return Path.GetFullPath(Path.Combine(".", "Games", "CounterStrikeConditionZero") + Path.DirectorySeparatorChar); } }



        //   public static Dictionary<ulong, bool> gIsGameServerOnline { get; set; } = new Dictionary<ulong, bool>();
        public static Dictionary<string, string> gIsGameServerOnline { get; set; } = new Dictionary<string, string>();


        public static bool SaveSettingsFile()
        {

            File.WriteAllText(gSettingsFile, JsonConvert.SerializeObject(gSettings));
            return true;
        }

        public static SettingsModel LoadSettingsFile()
        {
            return JsonConvert.DeserializeObject<SettingsModel>(File.ReadAllText(gSettingsFile));
        }
    }
}
