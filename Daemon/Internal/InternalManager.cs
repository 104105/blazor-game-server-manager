﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using WindowsFirewallHelper;

namespace Daemon.Internal
{
    public class InternalManager
    {
        #region Variables 
        public static Dictionary<int, ulong> gProcessIDToKey = new Dictionary<int, ulong>();

          #endregion

        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);
        [DllImport("user32.dll")]
        [return: MarshalAs  (UnmanagedType.Bool)]
        public static extern bool SetForegroundWindow(IntPtr WindowHandle);

        [DllImport("user32.dll", SetLastError = true)]
        internal static extern bool ShowWindowAsync(IntPtr windowHandle, int nCmdShow);



        #region IO Stream Handlers
        public static void Process_Exited(object sender, EventArgs e)
        {
            ulong key = gProcessIDToKey[((Process)sender).Id];
            ProcessIOManager.OutputReceived(key, "Finished...");
        }

        public static void OutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            ulong key = gProcessIDToKey[((Process)sender).Id];
            ProcessIOManager.OutputReceived(key, e.Data);
        }

        public static void ErrorOutputDataReceived(object sender, DataReceivedEventArgs e)
        {
            ulong key = gProcessIDToKey[((Process)sender).Id];
            ProcessIOManager.ErrorOutputReceived(key, e.Data);
        }
        #endregion


    /**    public static void InstallFake()
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo("cmd.exe");
            Console.WriteLine(processStartInfo.Arguments);
            processStartInfo.RedirectStandardInput = true;
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.UseShellExecute = false;
            processStartInfo.CreateNoWindow = false;



            Process proc = Process.Start(processStartInfo);
            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();

            gProcessIDToKey.Add(proc.Id,0);
            ProcessIOManager.AddNewStream(0);

            proc.OutputDataReceived += OutputDataReceived;
            proc.ErrorDataReceived += ErrorOutputDataReceived;

            ShowWindow(proc.MainWindowHandle, 1);

            // Use a WIN API command to bring the command line to front
            SetForegroundWindow(proc.MainWindowHandle);
            // Send a keystore to re-display the STATUS of the worker
            //  InputSimulator.              // Hide the window again.
            WindowsInput.InputSimulator sim = new WindowsInput.InputSimulator();
            sim.Keyboard.TextEntry("help");
            sim.Keyboard.KeyDown(WindowsInput.Native.VirtualKeyCode.RETURN);
            sim.Keyboard.KeyUp(WindowsInput.Native.VirtualKeyCode.RETURN);

          //  ShowWindow(proc.MainWindowHandle, 0);


            //proc.StandardInput.WriteLine("help");
        }*/

        #region Helpers


        public static ulong DownloadSteamGame(string installDir, long appID, EventHandler callBack,string appConfig = "")
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo(GlobalVariables.gSteamCMDExePath, string.Format("+force_install_dir {0} +login anonymous +app_update {1} {2} validate +quit", installDir, appID, !string.IsNullOrEmpty(appConfig) ? "+app_set_config " + "\""+ appConfig+"\"" : string.Empty));

            Process proc = Process.Start(processStartInfo);


            proc.EnableRaisingEvents = true;

            proc.Exited += callBack;


            return GlobalVariables.gSettings.GetUniqueGameServerID();
        }

       
        public static void EnterConsoleCommand(Process proc, string cmd)
        {
            if (GlobalVariables.gSettings.isWindows)
            {
                ShowWindowAsync(proc.MainWindowHandle, 10);

                ShowWindowAsync(proc.MainWindowHandle, 5);

                // Use a WIN API command to bring the command line to front
                SetForegroundWindow(proc.MainWindowHandle);

                Console.WriteLine(proc.ProcessName);
                Console.WriteLine(proc.Id);
                Console.WriteLine(proc.MainWindowTitle);

                // Send a keystore to re-display the STATUS of the worker
                //              // Hide the window again.
                //WindowsInput.InputSimulator sim = new WindowsInput.InputSimulator();
                //sim.Keyboard.TextEntry(cmd);
                //sim.Keyboard.KeyDown(WindowsInput.Native.VirtualKeyCode.RETURN);
                //sim.Keyboard.KeyUp(WindowsInput.Native.VirtualKeyCode.RETURN);

            //    ShowWindow(proc.MainWindowHandle, 0);
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                Console.WriteLine("Error: Linux Console Command Simulation Not Implemented!");
            }
        }

        public static Process StartGameServerProcess(ulong gameServerId, string exePath, string args)
        {
            ProcessStartInfo processStartInfo = new ProcessStartInfo(exePath, args);
            Console.WriteLine(processStartInfo.Arguments);
            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.RedirectStandardError = true;
            processStartInfo.UseShellExecute = false;
            processStartInfo.WorkingDirectory = Path.GetDirectoryName(exePath);

            Process proc = Process.Start(processStartInfo);
            proc.BeginErrorReadLine();
            proc.BeginOutputReadLine();

            gProcessIDToKey.Add(proc.Id, gameServerId);

            if (GlobalVariables.gIsGameServerOnline.ContainsKey(gameServerId.ToString()))
            {
                GlobalVariables.gIsGameServerOnline[gameServerId.ToString()] = true.ToString();
            }else
            {
                GlobalVariables.gIsGameServerOnline.Add(gameServerId.ToString(), true.ToString());
            }

            ProcessIOManager.AddNewStream(gameServerId);

            proc.OutputDataReceived += OutputDataReceived;
            proc.ErrorDataReceived += ErrorOutputDataReceived;

            GlobalVariables.gRunningProcesses.Add(proc);
            ShowWindow(proc.MainWindowHandle, 0);
            return proc;
        }


        public static void AddFirewallException(string exePath,string ruleName)
        {
            var rule = FirewallManager.Instance.CreateApplicationRule(FirewallProfiles.Private | FirewallProfiles.Public, ruleName, FirewallAction.Allow, exePath);

            rule.Direction = FirewallDirection.Outbound | FirewallDirection.Inbound;

            //Check if Firewall Rule exists
            if (!FirewallManager.Instance.Rules.Contains(rule))
            {
                FirewallManager.Instance.Rules.Add(rule);
                Console.WriteLine(FirewallManager.Instance.IsSupported  + " is firewall supported " );
                Console.WriteLine("Add new firewall rule");
            }else
            {
                Console.WriteLine("already contains exe rule");
            }
        }

        public static int? GameServerIDToProcessID(ulong gameServerId)
        {
            foreach (var item in gProcessIDToKey)
            {
                if (item.Value == gameServerId)
                {
                    return item.Key;
                }
            }
            return null;
        }

        public static void DirectoryCopy(string sourceDirName, string destDirName, bool copySubDirs)
        {
            // Get the subdirectories for the specified directory.
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);

            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the destination directory doesn't exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }

            // Get the files in the directory and copy them to the new location.
            FileInfo[] files = dir.GetFiles();
            foreach (FileInfo file in files)
            {
                string temppath = Path.Combine(destDirName, file.Name);
                file.CopyTo(temppath, true);
            }

            // If copying subdirectories, copy them and their contents to new location.
            if (copySubDirs)
            {
                foreach (DirectoryInfo subdir in dirs)
                {
                    string temppath = Path.Combine(destDirName, subdir.Name);
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

        #endregion
    }
}
