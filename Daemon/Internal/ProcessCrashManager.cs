﻿using Daemon.Internal.Games;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Daemon.Internal
{
    public class ProcessCrashManager
    {
        public static void CheckIfGameServersCrashed(object sender, System.Timers.ElapsedEventArgs e)
        {
            foreach (var item in GlobalVariables.gRunningProcesses)
            {
                if (item.HasExited)
                {
                    ProcessIOManager.ErrorOutputReceived(InternalManager.gProcessIDToKey[item.Id], " Game Server Crashed...restarting");


                    foreach (var gameServer in GlobalVariables.gSettings.gTerrariaGameServers)
                    {
                        if (gameServer.UniqueGameServerID == InternalManager.gProcessIDToKey[item.Id])
                        {
                            TerrariaManager.StartTerrariaServer(gameServer);
                            break;
                        }
                    }


                    foreach (var gameServer in GlobalVariables.gSettings.gCounterStrikeGoGameServers)
                    {
                        if (gameServer.UniqueGameServerID == InternalManager.gProcessIDToKey[item.Id])
                        {
                            CSGOManager.StartCSGoServer(gameServer);
                            break;
                        }
                    }

                }
            }
        }
    }
}
