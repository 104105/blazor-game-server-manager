﻿using Newtonsoft.Json;
using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;

namespace Daemon.Internal
{
    public class StatManager
    {

        public static void PollDedicatedStats(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (GlobalVariables.gDedidicatedStas.Count > 34559)
            {
                Console.WriteLine("Flush stats");
                //flush
                List<Stastistic> archiveStats = new List<Stastistic>(GlobalVariables.gDedidicatedStas.GetRange(0, 17280));
                GlobalVariables.gDedidicatedStas.RemoveRange(0, 17280);
                File.WriteAllText(Path.Combine(".","Stats", "Stats_" + DateTime.UtcNow.Day + DateTime.UtcNow.Month + DateTime.UtcNow.Year + ".json"), JsonConvert.SerializeObject(archiveStats));
            }

            var cpuCounter = new PerformanceCounter("Process", "% Processor Time", Process.GetCurrentProcess().ProcessName);
            float cpuUsage = (cpuCounter.NextValue() / Environment.ProcessorCount);

            PerformanceCounter memoryCounter = new PerformanceCounter("Memory", "Available MBytes");
            float memoryUsage = memoryCounter.NextValue();

            long networkDownload = 0;
            long networkUpload = 0;

            System.Net.NetworkInformation.NetworkInterface[] nics;
            nics = System.Net.NetworkInformation.NetworkInterface.GetAllNetworkInterfaces();

            foreach (var nic in nics)
            {
                networkDownload += nic.GetIPv4Statistics().BytesReceived;
                networkUpload += nic.GetIPv4Statistics().BytesSent;
            }

        

            var dedStat = new Stastistic()
            {
                CaptureTime = DateTime.UtcNow,
                CPUUtilization = cpuUsage,
                MemoryUsage = memoryUsage,
                NetworkDownloadUsage = networkDownload - GlobalVariables.gSettings.gLastTotalBytesDownloaded,
                NetworkUploadUsage = networkUpload - GlobalVariables.gSettings.gLastTotalBytesUploaded,
                isGameServerOnline = GlobalVariables.gIsGameServerOnline
            };

            GlobalVariables.gDedidicatedStas.Add(dedStat);

            GlobalVariables.gSettings.gLastTotalBytesDownloaded = networkDownload;
            GlobalVariables.gSettings.gLastTotalBytesUploaded =networkUpload;

        }

        static double GetNetworkUtilization(string networkCard)
        {

            const int numberOfIterations = 10;
            PerformanceCounter bandwidthCounter = new PerformanceCounter("Network Interface", "Current Bandwidth", networkCard);
            float bandwidth = bandwidthCounter.NextValue();
            PerformanceCounter dataSentCounter = new PerformanceCounter("Network Interface", "Bytes Sent/sec", networkCard);
            PerformanceCounter dataReceivedCounter = new PerformanceCounter("Network Interface", "Bytes Received/sec", networkCard);
            float sendSum = 0;
            float receiveSum = 0;

            for (int index = 0; index < numberOfIterations; index++)
            {
                sendSum += dataSentCounter.NextValue();
                receiveSum += dataReceivedCounter.NextValue();
            }

            float dataSent = sendSum;
            float dataReceived = receiveSum;
            double utilization = (8 * (dataSent + dataReceived)) / (bandwidth * numberOfIterations) * 100;
            return utilization;
        }

     
    }
}
