﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class SevenDays2DieManager
    {
        #region 7Days2Die
        public static ulong Install7Days2DieServer(_7Days2DieGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.g7Days2DieBaseDirectory, 294420, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(_7Days2DieGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.g7Days2DieBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.g7Days2DieGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            Start7Days2DieServer(data);
        }

        public static void Start7Days2DieServer(_7Days2DieGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            //string args = string.Format("-game csgo -console -usercon -port {0} +sv_setsteamaccount {1} -net_port_try 1 + game_type {2} +game_mode {3} +mapgroup {4} +map {5}", 28015, data.GSLT, data.GameType, data.GameMode, data.MapGroup, data.StartingMap);

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, "-batchmode");



        }

        private static string GetExecutablePath(_7Days2DieGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);
            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + "startdedicated.bat";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath + "startserver.sh";
            }
            
            return exePath;
        }

        public static string GetDirectoryPath(_7Days2DieGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        #endregion

    }
}
