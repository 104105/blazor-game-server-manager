﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class RustManager
    {
        #region Rust Manager
        public static ulong InstallRustServer(RustGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gRustBaseDirectory, 258550, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(RustGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gRustBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gRustGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartRustServer(data);
        }

        public static void StartRustServer(RustGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format("-batchmode");

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(RustGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(RustGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);
            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + "RustDedicated.exe";// ";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath + "RustDedicated";
            }

            return exePath;
        }

    
        #endregion

    }
}
