﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class CSGOManager
    {
        #region CSGO
        public static ulong InstallCSGoServer(CounterStrikeGOGameServer data)
        {
            //ProcessStartInfo processStartInfo = new ProcessStartInfo(GlobalVariables.gSteamCMDExePath, string.Format("+force_install_dir {0} +login anonymous +app_update 740 validate +quit", InternalManager.gCounterStrikeGOBaseDirectory));

            //Process proc = Process.Start(processStartInfo);


            //proc.EnableRaisingEvents = true;

            //data.UniqueGameServerID = GlobalVariables.gSettings.GetUniqueGameServerID();
            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gCounterStrikeGOBaseDirectory,740,new EventHandler((object sender, System.EventArgs e) => {
                FinishedSteamGameInstall(data);
            }));

        
            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(CounterStrikeGOGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gCounterStrikeGOBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gCounterStrikeGoGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartCSGoServer(data);
        }

        public static void StartCSGoServer(CounterStrikeGOGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format("-game csgo -console -usercon -port {0} +sv_setsteamaccount {1} -net_port_try 1 + game_type {2} +game_mode {3} +mapgroup {4} +map {5}", 28015, data.GSLT, data.GameType, data.GameMode, data.MapGroup, data.StartingMap);

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);



        }

        private static string GetExecutablePath(CounterStrikeGOGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);
            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + "srcds.exe";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath + "srcds_run";
            }

            return exePath;
        }

        public static string GetDirectoryPath(CounterStrikeGOGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        #endregion

    }
}
