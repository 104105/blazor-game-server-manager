﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class SpaceEngineersManager
    {
        #region Space Engineers
        public static ulong InstallGameServer(SpaceEngineersGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gSpaceEngineersBaseDirectory, 298740, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(SpaceEngineersGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gSpaceEngineersBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gSpaceEngineersGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartGameServer(data);
        }

        public static void StartGameServer(SpaceEngineersGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

          //  string args = string.Format("-console -game left4dead2 -secure ");

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, "-console");

        }

        public static string GetDirectoryPath(SpaceEngineersGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(SpaceEngineersGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);


            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + Path.Combine("DedicatedServer64","SpaceEngineersDedicated.exe");
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
               // exePath = directoryPath  + "SquadGameServer.sh"; 
            }

            return exePath;
        }

    
        #endregion

    }
}
