﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class EmpyrionGalacticSurvivalManager
    {
        #region Empyrion Galactic Survival
        public static ulong InstallGameServer(EmpyrionGalacticSurvivalGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gEmpyrionGalacticSurvivalBaseDirectory, 530870, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(EmpyrionGalacticSurvivalGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gEmpyrionGalacticSurvivalBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gEmpyrionGalacticSurvivalGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartGameServer(data);
        }

        public static void StartGameServer(EmpyrionGalacticSurvivalGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Empty;//string.Format("-console -game left4dead2 -secure ");

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(EmpyrionGalacticSurvivalGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(EmpyrionGalacticSurvivalGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);



            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + "EmpyrionDedicated_NoGraphics.cmd";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath  + "EmpyrionDedicated_NoGraphics.cmd"; 
            }

            return exePath;
        }

    
        #endregion

    }
}
