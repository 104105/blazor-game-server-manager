﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class KillingFloor2Manager
    {
        #region Killing Floor 2
        public static ulong InstalKillingFloor2Server(KillingFloor2GameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gKillingFloor2BaseDirectory, 232130, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(KillingFloor2GameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gKillingFloor2BaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gKillingFloor2GameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartKillingFloor2Server(data);
        }

        public static void StartKillingFloor2Server(KillingFloor2GameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format(string.Empty);

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(KillingFloor2GameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(KillingFloor2GameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);
            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + Path.Combine("Binaries", "Win64") + "KFServer.exe";// \Binaries\Win64\KFGame.exe "ShooterGameServer.exe";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath + Path.Combine("Binaries", "Win64") + "KFGameSteamServer.bin.x86_64"; 
            }

            return exePath;
        }

    
        #endregion

    }
}
