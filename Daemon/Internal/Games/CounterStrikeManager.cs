﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class CounterStrikeManager

    {
        #region Counter-Strike
        public static ulong InstallGameServer(CounterStrikeGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gCounterStrikeBaseDirectory, 90, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(CounterStrikeGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gCounterStrikeBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gCounterStrikeGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartGameServer(data);
        }

        public static void StartGameServer(CounterStrikeGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format("-game cstrike -console");

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(CounterStrikeGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(CounterStrikeGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);



            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + "hlds.exe";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath  + "hlds"; 
            }

            return exePath;
        }

    
        #endregion

    }
}
