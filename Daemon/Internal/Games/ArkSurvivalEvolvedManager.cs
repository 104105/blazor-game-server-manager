﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class ArkSurvivalEvolvedManager
    {
        #region ArkSurvivalEvolved
        public static ulong InstallArkSEServer(ArkGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gArkSurvivalEvolvedBaseDirectory, 376030, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(ArkGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gArkSurvivalEvolvedBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gArkGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartArkSEServer(data);
        }

        public static void StartArkSEServer(ArkGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format("TheIsland?listen?SessionName={0}" ,data.ServerName);

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(ArkGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(ArkGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);
            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + Path.Combine("ShooterGame","Binaries","Win64","ShooterGameServer.exe");
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath + Path.Combine("ShooterGame", "Binaries", "Linux", "ShooterGameServer");
            }


            return exePath;
        }

    
        #endregion

    }
}
