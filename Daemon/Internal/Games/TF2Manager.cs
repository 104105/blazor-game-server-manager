﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class TF2Manager
    {
        #region Team Fortress 2 Manager
        public static ulong InstallTF2Server(TeamFortress2GameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gTF2BaseDirectory, 232250, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(TeamFortress2GameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gTF2BaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gTF2GameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartTF2Server(data);
        }

        public static void StartTF2Server(TeamFortress2GameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format("-console -game tf +sv_pure 1" );

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(TeamFortress2GameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(TeamFortress2GameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);
            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath +  "srcds.exe";// ";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath + "srcds_run";
            }

            return exePath;
        }

    
        #endregion

    }
}
