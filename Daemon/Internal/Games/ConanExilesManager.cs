﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class ConanExilesManager
    {
        #region Conan Exiles
        public static ulong InstallConanExilesGameServer(ConanExilesGameServer data)
        {
            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gConanExilesBaseDirectory, 443030, new EventHandler((object sender, System.EventArgs e) => {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(ConanExilesGameServer data)
        {
            Console.WriteLine("Copy Conan Exiles Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gConanExilesBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gConanExilesGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartConanExilesGameServer(data);
        }

        public static void StartConanExilesGameServer(ConanExilesGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format("-log");

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);



        }

        private static string GetExecutablePath(ConanExilesGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);
            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + "ConanSandboxServer.exe";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
              
            }

            return exePath;
        }


        public static string GetDirectoryPath(ConanExilesGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";

        }

        #endregion
    }
}

