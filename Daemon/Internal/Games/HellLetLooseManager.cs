﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class HellLetLooseManager
    {
        #region Hell Let Loose
        public static ulong InstallGameServer(HellLetLooseGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gHellLetLooseBaseDirectory, 822500, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(HellLetLooseGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gHellLetLooseBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gHellLetLooseGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartGameServer(data);
        }

        public static void StartGameServer(HellLetLooseGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format(string.Empty);

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(HellLetLooseGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(HellLetLooseGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);



            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + " StartServer.bat";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
          //      exePath = directoryPath  + "srcds_run"; 
            }

            return exePath;
        }

    
        #endregion

    }
}
