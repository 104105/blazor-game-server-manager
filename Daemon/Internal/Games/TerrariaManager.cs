﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class TerrariaManager
    {
        #region Terraria
        public static ulong InstallTerrariaServer(TerrariaGameServer data)
        {
            try
            {
                GlobalVariables.gWebClient.DownloadFile("https://terraria.org/system/dedicated_servers/archives/000/000/039/original/terraria-server-1405.zip", GlobalVariables.gTerrariaBaseDirectory + "terraria-server.1405.zip");

                ZipFile.ExtractToDirectory(GlobalVariables.gTerrariaBaseDirectory + "terraria-server.1405.zip", GlobalVariables.gTerrariaBaseDirectory, true);

                data.UniqueGameServerID = GlobalVariables.gSettings.GetUniqueGameServerID();
                Console.WriteLine(data.UniqueGameServerID);
                InternalManager.DirectoryCopy(GlobalVariables.gTerrariaBaseDirectory, GlobalVariables.gTerrariaGameServerDirectory + data.UniqueGameServerID + Path.DirectorySeparatorChar, true);
                data.WorldSavePath = GlobalVariables.gTerrariaWorldSavePath;
                data.ParseConfigfile();

                GlobalVariables.gSettings.gTerrariaGameServers.Add(data);
                GlobalVariables.SaveSettingsFile();
                StartTerrariaServer(data);

                return data.UniqueGameServerID;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                return 0;
            }
        }

        public static void StartTerrariaServer(TerrariaGameServer data)
        {
            try
            {
                string directoryPath = GetDirectoryPath(data);
                string exePath = string.Empty;

                if (GlobalVariables.gSettings.isWindows)
                {
                    var exeDir = directoryPath + "Windows" + Path.DirectorySeparatorChar;
                    File.WriteAllText(exeDir + "config.cfg", data.DefaultConfigFile);
                    exePath = exeDir + "TerrariaServer.exe";
                }
                else if (GlobalVariables.gSettings.isLinux)
                {
                    var exeDir = directoryPath + "Linux" + Path.DirectorySeparatorChar;
                    File.WriteAllText(exeDir + "config.cfg", data.DefaultConfigFile);
                    exePath = exeDir + "TerrariaServer";
                }

                InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

                InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, string.Format("-config {4} -port {0} -ip {1} -disableannouncementbox {2} -announcementboxrange {3} ", data.PortNumber, data.IPAddress, data.DisableAccouncementBox, data.AnnouncementBoxRange, Path.GetFullPath(directoryPath + "Windows" + Path.DirectorySeparatorChar + "config.cfg")));


            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }

        private static string GetDirectoryPath(TerrariaGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        #endregion

    }
}
