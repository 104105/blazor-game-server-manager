﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class SCPSecretLaboratoryManager
    {
        #region SCP Secret Laboratory
        public static ulong InstallGameServer(SCPSecretLaboratoryGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gSCPSecretLaboratoryBaseDirectory, 996560, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(SCPSecretLaboratoryGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gSCPSecretLaboratoryBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gSCPSecretLaboratoryGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartGameServer(data);
        }

        public static void StartGameServer(SCPSecretLaboratoryGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format("-batchmode ");

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(SCPSecretLaboratoryGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(SCPSecretLaboratoryGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);



            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + "LocalAdmin.exe";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath  + "LocalAdmin"; 
            }

            return exePath;
        }

    
        #endregion

    }
}
