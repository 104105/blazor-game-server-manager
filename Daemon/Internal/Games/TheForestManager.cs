﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class TheForestManager
    {
        #region The Forest
        public static ulong InstalTheForestServer(TheForestGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gTheForestBaseDirectory, 556450, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(TheForestGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gTheForestBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gTheForestGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartTheForestServer(data);
        }

        public static void StartTheForestServer(TheForestGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format("-batchmode");

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(TheForestGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(TheForestGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);
            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath +"TheForestDedicatedServer.exe";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                //exePath = directoryPath + Path.Combine("Binaries", "Win64") + "KFGameSteamServer.bin.x86_64"; 
            }

            return exePath;
        }

    
        #endregion

    }
}
