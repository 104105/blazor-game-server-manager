﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class SquadManager
    {
        #region Squad
        public static ulong InstallGameServer(SquadGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gSquadBaseDirectory, 403240, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(SquadGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gSquadBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gSquadGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartGameServer(data);
        }

        public static void StartGameServer(SquadGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

          //  string args = string.Format("-console -game left4dead2 -secure ");

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, string.Empty);

        }

        public static string GetDirectoryPath(SquadGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(SquadGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);



            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + "SquadGameServer.exe";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath  + "SquadGameServer.sh"; 
            }

            return exePath;
        }

    
        #endregion

    }
}
