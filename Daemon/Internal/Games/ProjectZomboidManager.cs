﻿using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Daemon.Internal.Games
{
    public class ProjectZomboidManager
    {
        #region Project Zomboid
        public static ulong InstallProjectZomboidServer(ProjectZomboidGameServer data)
        {

            data.UniqueGameServerID = InternalManager.DownloadSteamGame(GlobalVariables.gProjectZomboidBaseDirectory, 380870, new EventHandler((object sender, System.EventArgs e) =>
            {
                FinishedSteamGameInstall(data);
            }));


            return data.UniqueGameServerID;
        }


        public static void FinishedSteamGameInstall(ProjectZomboidGameServer data)
        {
            Console.WriteLine("Copy CS:GO Game Server Base Files");
            InternalManager.DirectoryCopy(GlobalVariables.gProjectZomboidBaseDirectory, GetDirectoryPath(data), true);
            Console.WriteLine("===END COPY===");

            GlobalVariables.gSettings.gProjectZomboidGameServers.Add(data);
            GlobalVariables.SaveSettingsFile();
            Console.WriteLine("===Add Game Server To Database===");

            StartProjectZomboidServer(data);
        }

        public static void StartProjectZomboidServer(ProjectZomboidGameServer data)
        {
            string exePath = GetExecutablePath(data);


            InternalManager.AddFirewallException(exePath, "GameServer " + data.UniqueGameServerID);

            string args = string.Format("-batchmode");

            var proc = InternalManager.StartGameServerProcess(data.UniqueGameServerID, exePath, args);

        }

        public static string GetDirectoryPath(ProjectZomboidGameServer data)
        {
            return $"{Path.GetFullPath(Path.Combine(".", "Games") + Path.DirectorySeparatorChar)}{data.UniqueGameServerID}{Path.DirectorySeparatorChar}";
        }

        private static string GetExecutablePath(ProjectZomboidGameServer data)
        {
            var exePath = string.Empty;
            var directoryPath = GetDirectoryPath(data);
            if (GlobalVariables.gSettings.isWindows)
            {
                exePath = directoryPath + "startServer64.bat";
            }
            else if (GlobalVariables.gSettings.isLinux)
            {
                exePath = directoryPath + "start-server.sh"; 
            }

            return exePath;
        }

    
        #endregion

    }
}
