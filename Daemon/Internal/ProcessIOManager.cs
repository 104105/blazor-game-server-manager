﻿using Daemon.Models.Local;
using SharedData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Daemon.Internal
{
    public class ProcessIOManager
    {
        static Dictionary<ulong, IOStream> m_IOStreams = new Dictionary<ulong, IOStream>();


        public static void AddNewStream(ulong key)
        {
            Console.WriteLine("Add new iostream " + key);
            m_IOStreams.Add(key, new IOStream());
        }


        public static void OutputReceived(ulong key, string line)
        {
            Console.WriteLine("key: " + key + "  " + line);
            m_IOStreams[key].UnSyncedLines.Add("Info - " +DateTime.UtcNow.ToString() + ": " +line);
            if(m_IOStreams[key].UnSyncedLines.Count > 150)
            {
                m_IOStreams[key].AllLines.AddRange(m_IOStreams[key].UnSyncedLines.GetRange(0, 49));
                m_IOStreams[key].UnSyncedLines.RemoveRange(0, 50);
             }

        }

        public static void ErrorOutputReceived(ulong key, string line)
        {
            Console.WriteLine("key: " + key + "  " + line);
            m_IOStreams[key].UnSyncedLines.Add("Error - " + DateTime.UtcNow.ToString() + ": " + line);
           
            if (m_IOStreams[key].UnSyncedLines.Count > 150)
            {
                m_IOStreams[key].AllLines.AddRange(m_IOStreams[key].UnSyncedLines.GetRange(0, 49));
                m_IOStreams[key].UnSyncedLines.RemoveRange(0, 50);
            }
        }


        public static List<string> GetLines(ulong key)
        {
            var stream = m_IOStreams[key];
            List<string> lines = new List<string>(stream.UnSyncedLines);
            stream.AllLines.AddRange(stream.UnSyncedLines);
            stream.UnSyncedLines.Clear();
             return lines;
        }

        public static List<string> GetAllLines(ulong key)
        {
            var stream = m_IOStreams[key];
            List<string> buffer = new List<string>();
            buffer.AddRange(stream.UnSyncedLines);

            if (buffer.Count < 25)
            {
                if (stream.AllLines.Count > 25)
                {
                    buffer.AddRange(stream.AllLines.GetRange(stream.AllLines.Count - 25, 25));
                }
                else if (stream.AllLines.Count > 0)
                {
                    buffer.AddRange(stream.AllLines.GetRange(0, stream.AllLines.Count));
                }
            }

            stream.AllLines.AddRange(stream.UnSyncedLines);
            List<string> lines = stream.AllLines;
            stream.UnSyncedLines.Clear();

            return buffer;
        }

      
    }
}
